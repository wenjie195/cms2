<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Name.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allName = getName($conn);
// $allCategory = getCategory($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Quotation | CMS" />
    <title>Quotation | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Issue A Quotation</h1> 
    <!-- <form action="utilities/topUpProductFunction.php" method="POST"> -->
    <!-- <form action="utilities/addProductFunction.php" method="POST" enctype="multipart/form-data"> -->
    <!-- <form action="adminViewInvoice.php" method="POST" enctype="multipart/form-data"> -->
    <form action="adminQuotationAdd.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Bill To : <a class="red-link" href='adminAddSupplierName.php'>Click To Add New Name</a></p>
            <select class="clean tele-input" value="<?php echo $allName[0]->getName();?>" name="supplier_name" id="supplier_name" required>
                <option value="">Please Select a Name</option>
                <?php
                for ($cntAA=0; $cntAA <count($allName) ; $cntAA++)
                {
                ?>
                    <option value="<?php echo $allName[$cntAA]->getName(); ?>"> 
                        <?php echo $allName[$cntAA]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>  
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Term</p>
            <input class="clean tele-input"  type="text" placeholder="Term" id="term" name="term">            
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Date</p>     
            <input class="clean tele-input"  type="date" placeholder="Unit Price" id="date" name="date" required> 
        </div> 

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">NEXT</button>

        <div class="clear"></div>
    </form>
</div>

<!-- <style>
.invoice-li{
	color:#264a9c;
	background-color:white;}
.invoice-li .hover1a{
	display:none;}
.invoice-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

</body>
</html>