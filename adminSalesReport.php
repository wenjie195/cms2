<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Sales.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getLeaveStatus($conn);
$productDetails = getSales($conn, " WHERE status = 'Generated' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Sales Report | CMS" />
    <title>Sales Report | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <!-- <h1 class="h1-title open">Quotation (Pending)</h1> -->
    <h1 class="h1-title open">Company Sales Report</h1>

    <a href='adminSalesPre.php'>
      <div class="blue-btn width175">Generate Monthly Sales Report</div>
    </a>
    

    <div class="clear"></div>

    <div class="width100 shipping-div2 margin-top40">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Years / Month</th>
                    <th>Total Sales</th>
                    <th>Nett Salary</th>
                    <th>EPF</th>
                    <th>Socso</th>
                    <th>EIS</th>
                    <th>PCB</th>
                    <th>Rental</th>
                    <th>Other Expenses</th>
                    <th>Nett Profit</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getYears();?> / <?php echo $productDetails[$cnt]->getMonth();?></td>
                            <td><?php echo $productDetails[$cnt]->getSales();?></td>
                            <td><?php echo $productDetails[$cnt]->getSalary();?></td>
                            <td><?php echo $productDetails[$cnt]->getEpf();?></td>
                            <td><?php echo $productDetails[$cnt]->getSocso();?></td>
                            <td><?php echo $productDetails[$cnt]->getEis();?></td>
                            <td><?php echo $productDetails[$cnt]->getPcb();?></td>
                            <?php $rental = $productDetails[$cnt]->getRental();?>
                            <td><?php echo number_format("$rental",2);?></td>

                            <td><?php echo $productDetails[$cnt]->getExpenses();?></td>

                            <?php $profit = $productDetails[$cnt]->getProfit();?>
                            <td><b><?php echo number_format("$profit",2);?></b></td>

                            <td>
                              <form action="utilities/adminSalesDeleteFunction.php" method="POST">
                                <button class="clean hover1 img-btn" type="submit" name="sales_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                  <img src="img/close.png" class="width100 hover1a" alt="Reject" title="Reject" >
                                  <img src="img/close2.png" class="width100 hover1b" alt="Reject" title="Reject">
                                </button>
                              </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.sales-li{
	color:#264a9c;
	background-color:white;}
.sales-li .hover1a{
	display:none;}
.sales-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Leave Application Submitted <br> Pending For Approval"; 
            // $messageType = "<H2>GFYS</H2> <br><br> Your Leave is NOT APPROVE !!! <br><br> HAHAHAHAHA ..!.."; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Insufficent Leave !!"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Fail To Apply Leave !!";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Leave Application Approved !!";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Leave Application Rejected !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !!","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>