
<link rel="shortcut icon" href="https://vidatechft.com/hosting-picture/favi-none.ico" type="image/x-icon">
<link rel="icon" href="https://vidatechft.com/hosting-picture/favi-none.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/foundation.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="css/rrpowered_notification_script.css?version=1.0.1">
<link rel="stylesheet" type="text/css" href="css/component.css">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@200;300;400;700&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css?version=1.3.5">
