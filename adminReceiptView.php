<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Receipt.php';
require_once dirname(__FILE__) . '/classes/ReceiptDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Receipt | CMS" />
    <title>Receipt | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar page1-div">
    <div class="text-center header-div">
<!--        <div class="left-header">
            <img src="img/gp.png" class="header-logo" alt="GP7" title="GP7">
        </div>-->
        <div class="right-header">
            <h3 class="invoice-company-name">ABC SDN. BHD.</h3>
            <h3 class="invoice-company-number invoice-company-number2 margin-bottom5">2021231013066(1369386-H)</h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number"></h3>
        </div>
    </div>
    <div class="clear"></div>
   
    <?php
    // echo $_POST['quotation_uid'];
    if(isset($_POST['quotation_uid']))
    {
    $conn = connDB();
    $quotation = getReceipt($conn,"WHERE name = ? ", array("name") ,array($_POST['quotation_uid']),"s");
    // $quotationDetails = getReceiptDetails($conn,"WHERE quotation_uid = ? ", array("quotation_uid") ,array($_POST['quotation_uid']),"s");
    $quotationDetails = getReceiptDetails($conn,"WHERE quotation_uid = ? AND status != 'Delete' ", array("quotation_uid") ,array($_POST['quotation_uid']),"s");
    // $orderId = $ordersDetails[0]->getId();
    ?>


		<div class="left-invoice-div">
        	<p class="ms-p">Bill To: <b><?php echo $quotation[0]->getBillTo();?></b></p>
        </div>
        <div class="right-invoice-div">
            <p class="ms-p"><b>Receipt</b></p>
            <p class="ms-p">Receipt No.: <b><?php echo $_POST['quotation_uid'];?></b></p>
            <p class="ms-p">Date: <b><?php echo $quotation[0]->getDate();?></b></p>
            
		</div>
        <div class="clear"></div>

        <div class="width100">

            <table class="shipping-table print-table" id="myTable">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>UOM</th>
                        <!-- <th>U/Price</th> -->
                        <th>Total Amount (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // $conn = connDB();
                    if($quotationDetails)
                    {   
                        for($cnt = 0;$cnt < count($quotationDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $quotationDetails[$cnt]->getProductName();?></td>
                                <td><?php echo $quotationDetails[$cnt]->getQuantity();?></td>
                                <td><?php echo $quotationDetails[$cnt]->getUom();?></td>
                                <!-- <td><?php echo $quotationDetails[$cnt]->getUnitPrice();?></td> -->
                                <td><?php echo $quotationDetails[$cnt]->getTotal();?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    $conn->close();
                    ?>
                 <tr>
                    <td colspan="4">Total:</td>
                    <td><b><?php echo $quotation[0]->getAmount();?></b></td>
                </tr>
                </tbody>
            </table>
			<p class="ms-p sign-line"></p>
            <p class="sign-line-width">Authorised Signature</p>         

            <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn no-print" onclick="window.print()">Generate PDF</button>

            <?php 
            $status = $quotation[0]->getStatus();
            if($status == 'Pending')
            {
            ?>
                <a href='adminInvoiceEdit.php?id=<?php echo $_POST['quotation_uid'];?>'>
                    <button class="clean red-btn margin-top30 fix300-btn no-print">
                        Edit Receipt
                        <input type="hidden" value="<?php echo $_POST['quotation_uid'];?>" name='quotation_uid' id="quotation_uid" readonly>
                    </button>
                </a>
            <?php
            }
            else
            {}
            ?>

            <p class="text-center pointer red-link no-print"><a href="./img/pdf-tutorial.jpg" data-fancybox class="red-link">Tutorial</a></p>
        </div>

    <?php
    }
    ?>
    
    <div class="clear"></div>

</div>

<!-- <style>
.invoice-li{
	color:#264a9c;
	background-color:white;}
.invoice-li .hover1a{
	display:none;}
.invoice-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

</body>
</html>