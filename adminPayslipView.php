<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Payslip | CMS" />
    <title>Payslip | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar page1-div">
    <div class="text-center header-div">
<!--        <div class="left-header">
            <img src="img/gp.png" class="header-logo" alt="GP7" title="GP7">
        </div>-->
        <div class="right-header">
            <h3 class="invoice-company-name">ABC SDN. BHD.</h3>
            <h3 class="invoice-company-number invoice-company-number2 margin-bottom5">2021231013066(1369386-H)</h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number"></h3>
        </div>
    </div>
    <div class="clear"></div>
	
   
    <?php
    // echo $_POST['quotation_uid'];
    if(isset($_POST['payment_uid']))
    {
    $conn = connDB();
    $payslipDetails = getPayment($conn,"WHERE uid = ? ", array("uid") ,array($_POST['payment_uid']),"s");
    // $orderId = $ordersDetails[0]->getId();
    ?>
		
        	<p class="ms-p"><b>PAYSLIP <u>SALARY ADVICE OF THE MONTH OF <?php echo $payslipDetails[0]->getMonth();?> <?php echo $payslipDetails[0]->getYear();?></u></b> </p>
           
            <p class="ms-p">Monthly Payroll</p>
        

        
        <div class="clear"></div>
        <div class="width100 overflow-auto">
		<table class="print-table1 width100 margin-top40">
        	<tbody>
        		<tr>
                	<td><b>Staff Name</b></td>
                    <td><?php echo $fullname = $payslipDetails[0]->getFullname();?></td>
                    <td><b>Designation</b></td>
                    <td><?php echo $payslipDetails[0]->getDesignation();?></td>
                </tr>
        		<tr>
                	<td><b>I/C Number</b></td>
                    <?php
                        $conn = connDB();
                        $userDetails = getUser($conn,"WHERE fullname = ? ", array("fullname") ,array($fullname),"s");
                        $icno = $userDetails[0]->getIcno();
                    ?>
                    <td><?php echo $icno;?></td>

                    <td><b>Status</b></td>
                    <td><?php echo $payslipDetails[0]->getStatus();?></td>
                </tr>       
        		<tr>
                	<td><b>Date Of Joining</b></td>
                    <td><?php echo $userDetails[0]->getJoinDate();?></td>
                    <td><b>Department</b></td>
                    <td><?php echo $payslipDetails[0]->getDepartment();?></td>
                </tr>                     
        		<tr>
                	<td><b>EPF No</b></td>
                    <td><?php echo $payslipDetails[0]->getEpfNo();?></td>
                    <td><b>Account Number</b></td>
                    <td><?php echo $payslipDetails[0]->getAccountNo();?></td>
                </tr>       
        		<tr>
                	<td><b>Income Tax No.</b></td>
                    <td><?php echo $payslipDetails[0]->getIncomeTaxNo();?></td>
                    <td><b>Bank</b></td>
                    <td><?php echo $payslipDetails[0]->getBank();?></td>
                </tr>                 
            </tbody>
        </table>
       </div>

       

        <div class="clear"></div>
        <div class="width100 overflow-auto">
		<table class="print-table2 width100 margin-top40">
        	<thead>
            	<th colspan="3">INCOME</th>
                <th colspan="3">DEDUCTION</th>
            </thead>
        	<tbody>
        		<tr>
                	<td colspan="2">BASIC PAY</td>
                    <td class="border-right text-right"><?php echo $payslipDetails[0]->getBasicPay();?></td>
                    <td colspan="2">EPF</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getEpf();?></td>                  
                </tr>
        		<tr>
                	<td colspan="2">Allowance</td>
                    <td class="border-right text-right"><?php echo $payslipDetails[0]->getAllowance();?></td>
                    <td colspan="2">PERKESO</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getPerkeso();?></td>                  
                </tr>          
        		<tr>
                	<td colspan="2">Commission</td>
                    <td class="border-right text-right"><?php echo $payslipDetails[0]->getCommission();?></td>
                    <td colspan="2">EIS</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getEis();?></td>                  
                </tr>                     
         		<tr>
                	<td colspan="2">Bonus</td>
                    <td class="border-right text-right"><?php echo $payslipDetails[0]->getBonus();?></td>
                    <td colspan="2">PCB</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getPcb();?></td>                  
                </tr>                  
         		<tr>
                	<td colspan="2">&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td colspan="2">Unpaid Leave</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getUnpaidLeave();?></td>                  
                </tr>  
         		<tr>
                	<td>Overtime</td>
                    <td><?php echo $payslipDetails[0]->getOvertimeHrs();?> hrs</td>
                    <td class="border-right text-right"><?php echo $payslipDetails[0]->getOvertimeCost();?></td>
                    <td colspan="2">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>                  
                </tr>                   
         		<tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                    <td class="text-right">&nbsp;</td>                  
                </tr>     
         		<tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td colspan="3"><b><u>Employer Contribution</u></b></td>               
                </tr>                              
         		<tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td>EPF</td>
                    <td>-</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getEpfComp();?></td>                  
                </tr>     
         		<tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td>PERKESO</td>
                    <td>-</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getPerkesoComp();?></td>                  
                </tr>                 
         		<tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td>EIS</td>
                    <td>-</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getEisComp();?></td>                  
                </tr>  
          		<tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td colspan="2" class="top-border">Gross Income</td>
                    <td class="text-right top-border"><?php echo $payslipDetails[0]->getGrossIncome();?></td>                  
                </tr>                 
          		<tr>
                	<td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="border-right text-right">&nbsp;</td>
                    <td colspan="2">Net Income</td>
                    <td class="text-right"><?php echo $payslipDetails[0]->getNetIncome();?></td>                  
                </tr>  
          		<tr>
                	<td colspan="2" class="top-border padding10">GROSS TOTAL</td>
                    <td class="text-right top-border padding10"><?php echo $payslipDetails[0]->getGrossTotal();?></td>
                    <td colspan="2" class="top-border padding10"><b>NET MONTH PAY</b></td>
                    <td class="text-right top-border padding10"><?php echo $payslipDetails[0]->getNetMonthPay();?></td>                  
                </tr>                 
                                                                               
			 </tbody>
        </table>
        </div>
        <div class="width100 margin-top40">




            	<div class="center-div2 overflow-auto">
                <p class="ms-p"><b>AS of <?php echo $date = date("M",strtotime($payslipDetails[0]->getDateCreated()));?> <?php echo $date = date("Y",strtotime($payslipDetails[0]->getDateCreated()));?></b></p>
           

            <!-- <table class="time-table"> -->
            <table class="print-table3 width100 margin-top10">
                <thead>
                    <tr>
                        <th><b>LEAVE SUMMARY</b></th>
                        <th><b>Entitle</b></th>
                        <th><b>Applied</b></th>
                        <th><b>Balanced</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Annual Leave</td>
                        <td><?php echo $payslipDetails[0]->getAlEntitle();?></td>
                        <td><?php echo $payslipDetails[0]->getAlApplied();?></td>
                        <td><?php echo $payslipDetails[0]->getAlBalance();?></td>
                    </tr>     
                    <tr>
                        <td>Medical Leave</td>
                        <td><?php echo $payslipDetails[0]->getMlEntitle();?></td>
                        <td><?php echo $payslipDetails[0]->getMlApplied();?></td>
                        <td><?php echo $payslipDetails[0]->getMlBalance();?></td>
                    </tr> 
                </tbody>
            </table>
			</div>
            <div class="clear"></div>

            <p class="ms-p text-center margin-top40">"This is a computer-generated document. No sugnature is required"</p>

            <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn no-print" onclick="window.print()">Generate PDF</button>
            <p class="text-center pointer red-link no-print"><a href="./img/pdf-tutorial.jpg" data-fancybox class="red-link">Tutorial</a></p>
        </div>

    <?php
    }
    ?>
    
    <div class="clear"></div>

</div>

<!-- <style>
.invoice-li{
	color:#264a9c;
	background-color:white;}
.invoice-li .hover1a{
	display:none;}
.invoice-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

</body>
</html>