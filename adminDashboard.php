<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getProduct($conn);
$productDetails = getUser($conn);
// $productDetails = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Dashboard | CMS" />
    <title>Admin Dashboard | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="h1-title open">Admin Dashboard</h1>

    <div class="clear"></div>

    <div class="big-four-input-container">
      <div class="input50-div">
        <p class="input-top-p">Staff Name</p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Staff Name" class="tele-four-input tele-input clean">
      </div>

      <div class="input50-div second-input50">
        <p class="input-top-p">IC Number</p>
        <input type="text" id="myInputB" onkeyup="myFunctionB()" placeholder="IC Number" class="tele-four-input tele-input clean">
      </div>

      <!-- <div class="three-input-div">
        <p class="input-top-p">Product Code</p>
        <input type="text" id="myInputC" onkeyup="myFunctionC()" placeholder="Product Code" class="tele-four-input tele-input clean">
      </div> -->
    </div>

    <div class="clear"></div>

    <div class="big-four-input-container">
      <form action="utilities/adminLeaveAddAllFunction.php" method="POST">
        <button class="clean blue-btn" type="submit">
          Add Leave +1 (ALL)
        </button>
      </form>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Staff Name</th>
                    <th>IC No</th>
                    <th>Contact</th>
                    <!-- <th>Total A/L</th> -->
                    <!-- <th>A/L Applied</th> -->
                    <th>Account Type</th>
                    <th>Edit</th>
                    <th>Edit Password</th>
                    <th>Annual Leave</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productName = $productDetails[$cnt]->getFullname();?></td>
                            <td><?php echo $productDetails[$cnt]->getIcno();?></td>
                            <td><?php echo $productDetails[$cnt]->getPhone();?></td>
                            <!-- <td><?php //echo $productDetails[$cnt]->getLeaveTotal();?></td> -->
                            <!-- <td><?php //echo $productDetails[$cnt]->getLeaveApplied();?></td> -->

                            <td>
                              <?php 
                                $userType = $productDetails[$cnt]->getUserType();
                                if($userType == 0)
                                {
                                  echo "Admin";
                                }
                                elseif($userType == 1)
                                {
                                  echo "Normal User";
                                }
                                elseif($userType == 2)
                                {
                                  echo "Marketing";
                                }
                                elseif($userType == 10)
                                {
                                  echo "<b>RESIGN</b>";
                                }
                                else
                                {}
                              ?>
                            </td>

                            <td>
                              <form action="adminStaffEdit.php" method="POST">
                                <button class="clean hover1 img-btn" type="submit" name="user_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                  <img src="img/edit2.png" class="width100 hover1a" >
                                  <img src="img/edit3.png" class="width100 hover1b" >
                                </button>
                              </form>
                            </td>

                            <td>
                              <form action="adminStaffEditPassowrd.php" method="POST">
                                <button class="clean hover1 img-btn" type="submit" name="user_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                  <img src="img/edit2.png" class="width100 hover1a" >
                                  <img src="img/edit3.png" class="width100 hover1b" >
                                </button>
                              </form>
                            </td>

                            <td>
                              <form action="utilities/adminLeaveAddFunction.php" method="POST">
                              <!-- <form action="#" method="POST"> -->
                                <button class="clean blue-btn" type="submit" name="user_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                    Add (+1)
                                </button>
                              </form>
                            </td>

                            <td>
                              <form action="utilities/adminStaffDeleteFunction.php" method="POST">
                                <button class="clean blue-btn" type="submit" name="user_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                    Delete/Resign
                                </button>
                              </form>
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.dashboard-li{
	color:#264a9c;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php unset($_SESSION['quotation_session']); unset($_SESSION['invoice_session']); unset($_SESSION['url']);?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Staff Added !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Password Updated !"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Annual Leave Added !";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "All Staff's Annual Leave Added !";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Update Staff as RESIGN !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>