<?php
class User {
    /* Member variables */
    var $id,$uid,$username,$email,$password,$salt,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$leaveTotal,$leaveApplied,$joinDate,
            $epf,$epfNo,$sosco,$soscoNo,$lhdn,$lhdnNo,$loginType,$userType,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $uid
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return mixed
     */
    public function getIcno()
    {
        return $this->icno;
    }

    /**
     * @param mixed $icno
     */
    public function setIcno($icno)
    {
        $this->icno = $icno;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }



    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getEmergencyContact()
    {
        return $this->emergencyContact;
    }

    /**
     * @param mixed $emergencyContact
     */
    public function setEmergencyContact($emergencyContact)
    {
        $this->emergencyContact = $emergencyContact;
    }

    /**
     * @return mixed
     */
    public function getEmergencyPpl()
    {
        return $this->emergencyPpl;
    }

    /**
     * @param mixed $emergencyPpl
     */
    public function setEmergencyPpl($emergencyPpl)
    {
        $this->emergencyPpl = $emergencyPpl;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getAllowance()
    {
        return $this->allowance;
    }

    /**
     * @param mixed $allowance
     */
    public function setAllowance($allowance)
    {
        $this->allowance = $allowance;
    }

    /**
     * @return mixed
     */
    public function getLeaveTotal()
    {
        return $this->leaveTotal;
    }

    /**
     * @param mixed $leaveTotal
     */
    public function setLeaveTotal($leaveTotal)
    {
        $this->leaveTotal = $leaveTotal;
    }

    /**
     * @return mixed
     */
    public function getLeaveApplied()
    {
        return $this->leaveApplied;
    }

    /**
     * @param mixed $leaveApplied
     */
    public function setLeaveApplied($leaveApplied)
    {
        $this->leaveApplied = $leaveApplied;
    }

    /**
     * @return mixed
     */
    public function getJoinDate()
    {
        return $this->joinDate;
    }

    /**
     * @param mixed $joinDate
     */
    public function setJoinDate($joinDate)
    {
        $this->joinDate = $joinDate;
    }

    /**
     * @return mixed
     */
    public function getEpf()
    {
        return $this->epf;
    }

    /**
     * @param mixed $epf
     */
    public function setEpf($epf)
    {
        $this->epf = $epf;
    }

    /**
     * @return mixed
     */
    public function getEpfNo()
    {
        return $this->epfNo;
    }

    /**
     * @param mixed $epfNo
     */
    public function setEpfNo($epfNo)
    {
        $this->epfNo = $epfNo;
    }

    /**
     * @return mixed
     */
    public function getSosco()
    {
        return $this->sosco;
    }

    /**
     * @param mixed $sosco
     */
    public function setSosco($sosco)
    {
        $this->sosco = $sosco;
    }

    /**
     * @return mixed
     */
    public function getSoscoNo()
    {
        return $this->soscoNo;
    }

    /**
     * @param mixed $soscoNo
     */
    public function setSoscoNo($soscoNo)
    {
        $this->soscoNo = $soscoNo;
    }

    /**
     * @return mixed
     */
    public function getLhdn()
    {
        return $this->lhdn;
    }

    /**
     * @param mixed $lhdn
     */
    public function setLhdn($lhdn)
    {
        $this->lhdn = $lhdn;
    }

    /**
     * @return mixed
     */
    public function getLhdnNo()
    {
        return $this->lhdnNo;
    }

    /**
     * @param mixed $lhdnNo
     */
    public function setLhdnNo($lhdnNo)
    {
        $this->lhdnNo = $lhdnNo;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","email","password","salt","fullname","ic_no","phone","address","emergency_contact","emergency_ppl","salary","allowance",
                            "leave_total","leave_applied","join_date","epf","epf_no","sosco","sosco_no","lhdn","lhdn_no","login_type","user_type","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$username,$email,$password,$salt,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,
                            $leaveTotal,$leaveApplied,$joinDate,$epf,$epfNo,$sosco,$soscoNo,$lhdn,$lhdnNo,$loginType,$userType,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setSalt($salt);

            $user->setFullname($fullname);
            $user->setIcno($icno);

            $user->setPhone($phone);

            $user->setAddress($address);
            $user->setEmergencyContact($emergencyContact);
            $user->setEmergencyPpl($emergencyPpl);
            $user->setSalary($salary);
            $user->setAllowance($allowance);
            $user->setLeaveTotal($leaveTotal);
            $user->setLeaveApplied($leaveApplied);
            $user->setJoinDate($joinDate);
            $user->setEpf($epf);
            $user->setEpfNo($epfNo);
            $user->setSosco($sosco);
            $user->setSoscoNo($soscoNo);
            $user->setLhdn($lhdn);
            $user->setLhdnNo($lhdnNo);

            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
