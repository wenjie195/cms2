<?php
class Payment {
    /* Member variables */
    var $id,$uid,$userUid,$fullname,$designation,$icno,$status,$joinDate,$department,$epfNo,$accountNo,$incomeTaxNo,$bank,$basicPay,$allowance,$commission,$bonus,
            $overtimeHrs,$overtimeCost,$epf,$perkeso,$eis,$pcb,$unpaidLeave,$salaryAfterUL,$epfComp,$perkesoComp,$eisComp,$grossIncome,$netIncome,$grossTotal,$netMonthPay,
                $alEntitle,$alApplied,$alBalance,$mlEntitled,$mlApplied,$mlBalance,$month,$year,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUserUid()
    {
        return $this->userUid;
    }

    /**
     * @param mixed $userUid
     */
    public function setUserUid($userUid)
    {
        $this->userUid = $userUid;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param mixed $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return mixed
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param mixed $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return mixed
     */
    public function getIcno()
    {
        return $this->icno;
    }

    /**
     * @param mixed $icno
     */
    public function setIcno($icno)
    {
        $this->icno = $icno;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getJoinDate()
    {
        return $this->joinDate;
    }

    /**
     * @param mixed $joinDate
     */
    public function setJoinDate($joinDate)
    {
        $this->joinDate = $joinDate;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return mixed
     */
    public function getEpfNo()
    {
        return $this->epfNo;
    }

    /**
     * @param mixed $epfNo
     */
    public function setEpfNo($epfNo)
    {
        $this->epfNo = $epfNo;
    }

    /**
     * @return mixed
     */
    public function getAccountNo()
    {
        return $this->accountNo;
    }

    /**
     * @param mixed $accountNo
     */
    public function setAccountNo($accountNo)
    {
        $this->accountNo = $accountNo;
    }

    /**
     * @return mixed
     */
    public function getIncomeTaxNo()
    {
        return $this->incomeTaxNo;
    }

    /**
     * @param mixed $incomeTaxNo
     */
    public function setIncomeTaxNo($incomeTaxNo)
    {
        $this->incomeTaxNo = $incomeTaxNo;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
    }

    /**
     * @return mixed
     */
    public function getBasicPay()
    {
        return $this->basicPay;
    }

    /**
     * @param mixed $basicPay
     */
    public function setBasicPay($basicPay)
    {
        $this->basicPay = $basicPay;
    }

    /**
     * @return mixed
     */
    public function getAllowance()
    {
        return $this->allowance;
    }

    /**
     * @param mixed $allowance
     */
    public function setAllowance($allowance)
    {
        $this->allowance = $allowance;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $commission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param mixed $bonus
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * @return mixed
     */
    public function getOvertimeHrs()
    {
        return $this->overtimeHrs;
    }

    /**
     * @param mixed $overtimeHrs
     */
    public function setOvertimeHrs($overtimeHrs)
    {
        $this->overtimeHrs = $overtimeHrs;
    }

    /**
     * @return mixed
     */
    public function getOvertimeCost()
    {
        return $this->overtimeCost;
    }

    /**
     * @param mixed $overtimeCost
     */
    public function setOvertimeCost($overtimeCost)
    {
        $this->overtimeCost = $overtimeCost;
    }

    /**
     * @return mixed
     */
    public function getEpf()
    {
        return $this->epf;
    }

    /**
     * @param mixed $epf
     */
    public function setEpf($epf)
    {
        $this->epf = $epf;
    }

    /**
     * @return mixed
     */
    public function getPerkeso()
    {
        return $this->perkeso;
    }

    /**
     * @param mixed $perkeso
     */
    public function setPerkeso($perkeso)
    {
        $this->perkeso = $perkeso;
    }

    /**
     * @return mixed
     */
    public function getEis()
    {
        return $this->eis;
    }

    /**
     * @param mixed $eis
     */
    public function setEis($eis)
    {
        $this->eis = $eis;
    }

    /**
     * @return mixed
     */
    public function getPcb()
    {
        return $this->pcb;
    }

    /**
     * @param mixed $pcb
     */
    public function setPcb($pcb)
    {
        $this->pcb = $pcb;
    }

    /**
     * @return mixed
     */
    public function getUnpaidLeave()
    {
        return $this->unpaidLeave;
    }

    /**
     * @param mixed $unpaidLeave
     */
    public function setUnpaidLeave($unpaidLeave)
    {
        $this->unpaidLeave = $unpaidLeave;
    }

    /**
     * @return mixed
     */
    public function getSalaryAfterUL()
    {
        return $this->salaryAfterUL;
    }

    /**
     * @param mixed $salaryAfterUL
     */
    public function setSalaryAfterUL($salaryAfterUL)
    {
        $this->salaryAfterUL = $salaryAfterUL;
    }

    /**
     * @return mixed
     */
    public function getEpfComp()
    {
        return $this->epfComp;
    }

    /**
     * @param mixed $epfComp
     */
    public function setEpfComp($epfComp)
    {
        $this->epfComp = $epfComp;
    }

    /**
     * @return mixed
     */
    public function getPerkesoComp()
    {
        return $this->perkesoComp;
    }

    /**
     * @param mixed $perkesoComp
     */
    public function setPerkesoComp($perkesoComp)
    {
        $this->perkesoComp = $perkesoComp;
    }

    /**
     * @return mixed
     */
    public function getEisComp()
    {
        return $this->eisComp;
    }

    /**
     * @param mixed $eisComp
     */
    public function setEisComp($eisComp)
    {
        $this->eisComp = $eisComp;
    }

    /**
     * @return mixed
     */
    public function getGrossIncome()
    {
        return $this->grossIncome;
    }

    /**
     * @param mixed $grossIncome
     */
    public function setGrossIncome($grossIncome)
    {
        $this->grossIncome = $grossIncome;
    }

    /**
     * @return mixed
     */
    public function getNetIncome()
    {
        return $this->netIncome;
    }

    /**
     * @param mixed $netIncome
     */
    public function setNetIncome($netIncome)
    {
        $this->netIncome = $netIncome;
    }

    /**
     * @return mixed
     */
    public function getGrossTotal()
    {
        return $this->grossTotal;
    }

    /**
     * @param mixed $netIncome
     */
    public function setGrossTotal($grossTotal)
    {
        $this->grossTotal = $grossTotal;
    }

    /**
     * @return mixed
     */
    public function getNetMonthPay()
    {
        return $this->netMonthPay;
    }

    /**
     * @param mixed $netMonthPay
     */
    public function setNetMonthPay($netMonthPay)
    {
        $this->netMonthPay = $netMonthPay;
    }

    /**
     * @return mixed
     */
    public function getAlEntitle()
    {
        return $this->alEntitle;
    }

    /**
     * @param mixed $alEntitle
     */
    public function setAlEntitle($alEntitle)
    {
        $this->alEntitle = $alEntitle;
    }

    /**
     * @return mixed
     */
    public function getAlApplied()
    {
        return $this->alApplied;
    }

    /**
     * @param mixed $alApplied
     */
    public function setAlApplied($alApplied)
    {
        $this->alApplied = $alApplied;
    }

    /**
     * @return mixed
     */
    public function getAlBalance()
    {
        return $this->alBalance;
    }

    /**
     * @param mixed $alBalance
     */
    public function setAlBalance($alBalance)
    {
        $this->alBalance = $alBalance;
    }

    /**
     * @return mixed
     */
    public function getMlEntitle()
    {
        return $this->mlEntitled;
    }

    /**
     * @param mixed $mlEntitled
     */
    public function setMlEntitle($mlEntitled)
    {
        $this->mlEntitled = $mlEntitled;
    }

    /**
     * @return mixed
     */
    public function getMlApplied()
    {
        return $this->mlApplied;
    }

    /**
     * @param mixed $mlApplied
     */
    public function setMlApplied($mlApplied)
    {
        $this->mlApplied = $mlApplied;
    }

    /**
     * @return mixed
     */
    public function getMlBalance()
    {
        return $this->mlBalance;
    }

    /**
     * @param mixed $alApplied
     */
    public function setMlBalance($mlBalance)
    {
        $this->mlBalance = $mlBalance;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param mixed $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getPayment($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","user_uid","fullname","designation","ic_no","status","join_date","department","epf_no","account_no","income_tax_no","bank","basic_pay",
                            "allowance","commission","bonus","overtime_hrs","overtime_cost","epf","perkeso","eis","pcb","unpaid_leave","salary_after_ul","epf_comp",
                            "perkeso_comp","eis_comp","gross_income","net_income","gross_total","net_month_pay","al_entitle","al_applied","al_balance","ml_entitle",
                            "ml_applied","ml_balanced","month","year","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"payment");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */

        $stmt->bind_result($id,$uid,$userUid,$fullname,$designation,$icno,$status,$joinDate,$department,$epfNo,$accountNo,$incomeTaxNo,$bank,$basicPay,$allowance,
                            $commission,$bonus,$overtimeHrs,$overtimeCost,$epf,$perkeso,$eis,$pcb,$unpaidLeave,$salaryAfterUL,$epfComp,$perkesoComp,$eisComp,$grossIncome,
                            $netIncome,$grossTotal,$netMonthPay,$alEntitle,$alApplied,$alBalance,$mlEntitled,$mlApplied,$mlBalance,$month,$year,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Payment;
            $class->setId($id);
            $class->setUid($uid);
            $class->setUserUid($userUid);
            $class->setFullname($fullname);
            $class->setDesignation($designation);
            $class->setIcno($icno);
            $class->setStatus($status);
            $class->setJoinDate($joinDate);
            $class->setDepartment($department);
            $class->setEpfNo($epfNo);
            $class->setAccountNo($accountNo);
            $class->setIncomeTaxNo($incomeTaxNo);
            $class->setBank($bank);
            $class->setBasicPay($basicPay);
            $class->setAllowance($allowance);
            $class->setCommission($commission);
            $class->setBonus($bonus);
            $class->setOvertimeHrs($overtimeHrs);
            $class->setOvertimeCost($overtimeCost);
            $class->setEpf($epf);
            $class->setPerkeso($perkeso);
            $class->setEis($eis);
            $class->setPcb($pcb);
            $class->setUnpaidLeave($unpaidLeave);

            $class->setSalaryAfterUL($salaryAfterUL);

            $class->setEpfComp($epfComp);
            $class->setPerkesoComp($perkesoComp);
            $class->setEisComp($eisComp);
            $class->setGrossIncome($grossIncome);
            $class->setNetIncome($netIncome);
            $class->setGrossTotal($grossTotal);
            $class->setNetMonthPay($netMonthPay);
            $class->setAlEntitle($alEntitle);
            $class->setAlApplied($alApplied);
            $class->setAlBalance($alBalance);
            $class->setMlEntitle($mlEntitled);
            $class->setMlApplied($mlApplied);
            $class->setMlBalance($mlBalance);
            $class->setMonth($month);
            $class->setYear($year);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}