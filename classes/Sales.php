<?php  
class Sales {
    /* Member variables */
    var $id,$uid,$sales,$salary,$epf,$socso,$eis,$pcb,$rental,$expenses,$profit,$month,$years,$status,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param mixed $sales
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getEpf()
    {
        return $this->epf;
    }

    /**
     * @param mixed $epf
     */
    public function setEpf($epf)
    {
        $this->epf = $epf;
    }

    /**
     * @return mixed
     */
    public function getSocso()
    {
        return $this->socso;
    }

    /**
     * @param mixed $socso
     */
    public function setSocso($socso)
    {
        $this->socso = $socso;
    }

    /**
     * @return mixed
     */
    public function getEis()
    {
        return $this->eis;
    }

    /**
     * @param mixed $eis
     */
    public function setEis($eis)
    {
        $this->eis = $eis;
    }

    /**
     * @return mixed
     */
    public function getPcb()
    {
        return $this->pcb;
    }

    /**
     * @param mixed $pcb
     */
    public function setPcb($pcb)
    {
        $this->pcb = $pcb;
    }

    /**
     * @return mixed
     */
    public function getRental()
    {
        return $this->rental;
    }

    /**
     * @param mixed $rental
     */
    public function setRental($rental)
    {
        $this->rental = $rental;
    }

    /**
     * @return mixed
     */
    public function getExpenses()
    {
        return $this->expenses;
    }

    /**
     * @param mixed $expenses
     */
    public function setExpenses($expenses)
    {
        $this->expenses = $expenses;
    }

    /**
     * @return mixed
     */
    public function getProfit()
    {
        return $this->profit;
    }

    /**
     * @param mixed $profit
     */
    public function setProfit($profit)
    {
        $this->profit = $profit;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param mixed $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }


    /**
     * @return mixed
     */
    public function getYears()
    {
        return $this->years;
    }

    /**
     * @param mixed $years
     */
    public function setYears($years)
    {
        $this->years = $years;
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getSales($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","sales","salary","epf","socso","eis","pcb","rental","expenses","profit","month","years","status","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"sales");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$sales,$salary,$epf,$socso,$eis,$pcb,$rental,$expenses,$profit,$month,$years,$status,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Sales;
            $class->setId($id);
            $class->setUid($uid);

            $class->setSales($sales);
            $class->setSalary($salary);
            $class->setEpf($epf);
            $class->setSocso($socso);
            $class->setEis($eis);
            $class->setPcb($pcb);
            $class->setRental($rental);
            $class->setExpenses($expenses);
            $class->setProfit($profit);
            $class->setMonth($month);
            $class->setYears($years);

            $class->setStatus($status);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
