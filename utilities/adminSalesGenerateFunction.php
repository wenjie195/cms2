<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Payment.php';
require_once dirname(__FILE__) . '/../classes/Receipt.php';
require_once dirname(__FILE__) . '/../classes/Sales.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();
// $userUid = $_SESSION['uid'];

function addSales($conn,$uid,$totalSales,$nettSalary,$totalEpf,$totalSocso,$totalEis,$totalPcb,$rental,$expenses,$profit,$month,$year,$status)
{
     if(insertDynamicData($conn,"sales",array("uid","sales","salary","epf","socso","eis","pcb","rental","expenses","profit","month","years","status"),
          array($uid,$totalSales,$nettSalary,$totalEpf,$totalSocso,$totalEis,$totalPcb,$rental,$expenses,$profit,$month,$year,$status),"sssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $rental = rewrite($_POST['rental']);
     $expenses = rewrite($_POST['expenses']);
     $month = rewrite($_POST['month']);
     $year = rewrite($_POST['year']);

     // echo $rental = rewrite($_POST['rental'])."<br>";
     // echo $expenses = rewrite($_POST['expenses'])."<br>";
     // echo $month = rewrite($_POST['month']);
     // echo "<br>";
     // echo $year = rewrite($_POST['year']);
     // echo "<br>";
     // echo "<br>";
     // echo $month = 'Jan';
     // echo "<br>";
     // echo $year = 2021;
     // echo "<br>";
     // echo "<br>";

     // $productDetails = getReceipt($conn, " WHERE status = 'Closed' ");
     $productDetails = getReceipt($conn, " WHERE month = '$month' AND year = '$year' ");
     if($productDetails)
     {   
          $totalSales = 0; // initital
          for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
          {
               echo $receiptUid = $productDetails[$cnt]->getUid();
               echo "<br>";
               echo "<br>";
               // echo $sales = $productDetails[$cnt]->getAmount();
               // echo "<br>";
               // echo "<br>";
               echo $totalSales += $productDetails[$cnt]->getAmount();
               echo "<br>";
          }
     }

     $paymentDetails = getPayment($conn, " WHERE month = '$month' AND year = '$year' ");
     if($paymentDetails)
     {   
          $nettSalary = 0; // initital
          $epf = 0; // initital
          $epfComp = 0; // initital
          $perkeso = 0; // initital
          $perkesoComp = 0; // initital
          $eis = 0; // initital
          $eisComp = 0; // initital
          $totalPcb = 0; // initital

          for($cnt = 0;$cnt < count($paymentDetails) ;$cnt++)
          {
               echo $nettSalary += $paymentDetails[$cnt]->getNetMonthPay();
               echo "<br>";
               echo $epf += $paymentDetails[$cnt]->getEpf();
               echo "<br>";
               echo $epfComp += $paymentDetails[$cnt]->getEpfComp();
               echo "<br>";
               echo $perkeso += $paymentDetails[$cnt]->getPerkeso();
               echo "<br>";
               echo $perkesoComp += $paymentDetails[$cnt]->getPerkesoComp();
               echo "<br>";
               echo $eis += $paymentDetails[$cnt]->getEis();
               echo "<br>";
               echo $eisComp += $paymentDetails[$cnt]->getPerkesoComp();
               echo "<br>";
               echo $totalPcb += $paymentDetails[$cnt]->getPcb();
               echo "<br>";

               $totalEpf = $epf + $epfComp;
               $totalSocso = $perkeso + $perkesoComp;
               $totalEis = $eis + $eisComp;

               // echo $totalEpf = $epf + $epfComp;
               // echo "<br>";
               // echo $totalSocso = $perkeso + $perkesoComp;
               // echo "<br>";
               // echo $totalEis = $eis + $eisComp;
               // echo "<br>";
          }
     }

     $profit = $totalSales - $nettSalary - $totalEpf - $totalSocso - $totalEis - $rental - $expenses - $totalPcb ;

     $status = "Generated";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productName."<br>";
     // echo $category."<br>";

     if(addSales($conn,$uid,$totalSales,$nettSalary,$totalEpf,$totalSocso,$totalEis,$totalPcb,$rental,$expenses,$profit,$month,$year,$status))
     {
          // echo "success";
          header('Location: ../adminSalesReport.php');
     }
     else
     {
          echo "fail 2";
     }
}
else 
{
     header('Location: ../index.php');
}
?>