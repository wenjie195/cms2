<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Receipt.php';
require_once dirname(__FILE__) . '/../classes/ReceiptDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo $quotationSession = rewrite($_POST['receipt_session']);
     echo "<br>";

     $quotationDetails = getReceiptDetails($conn, " WHERE quotation_uid = ? ", array("quotation_uid") ,array($quotationSession),"s");
     if ($quotationDetails)
     {
         $totalAmount = 0; // initital
         for ($b=0; $b <count($quotationDetails) ; $b++)
         {
             $totalAmount += $quotationDetails[$b]->getTotal();
         }
     }
     echo $totalAmount;
     echo "<br>";

     if($quotationDetails)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($totalAmount)
          {
               array_push($tableName,"amount");
               array_push($tableValue,$totalAmount);
               $stringType .=  "s";
          }
          array_push($tableValue,$quotationSession);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"receipt"," WHERE name = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               // echo "SUCCESS";
               unset($_SESSION['receipt_session']);
               header('Location: ../adminReceiptAll.php');
          }
          else
          {
               echo "FAIL";
          }
     }
     else
     {
          echo "ERROR";
     }
}
else 
{
     header('Location: ../index.php');
}
?>