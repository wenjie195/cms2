<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/Offer.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $userUid = rewrite($_POST['user_uid']);

    $userType = 10;
  
    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");

    if ($userDetails)
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($userType)
        {
            array_push($tableName,"user_type");
            array_push($tableValue,$userType);
            $stringType .=  "i";
        }

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // header('Location: ../adminDashboard.php');
            $_SESSION['messageType'] = 1;
            header('Location: ../adminDashboard.php?type=5');
        }
        else
        {
            echo "<script>alert('FAIL !!');window.location='../adminDashboard.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminDashboard.php'</script>";
    }
}
else 
{
    header('Location: ../index.php');
}
?>