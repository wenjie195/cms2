<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Receipt.php';
require_once dirname(__FILE__) . '/../classes/ReceiptDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $itemUid = rewrite($_POST["item_uid"]);
    $quotationName = rewrite($_POST["quotation_name"]);
    $updateStatus = "Delete";

    $quotationDetails = getReceiptDetails($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");    

    if($quotationDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($updateStatus)
        {
            array_push($tableName,"status");
            array_push($tableValue,$updateStatus);
            $stringType .=  "s";
        }
        array_push($tableValue,$itemUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"receipt_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $updateQuotation = getReceiptDetails($conn," WHERE quotation_uid = ? AND status != 'Delete' ",array("quotation_uid"),array($quotationName),"s");    
            if ($updateQuotation)
            {
                $totalAmount = 0; // initital
                for ($b=0; $b <count($updateQuotation) ; $b++)
                {
                    $totalAmount += $updateQuotation[$b]->getTotal();
                }
            }

            if($updateQuotation)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($totalAmount)
                {
                    array_push($tableName,"amount");
                    array_push($tableValue,$totalAmount);
                    $stringType .=  "s";
                }        
                array_push($tableValue,$quotationName);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"receipt"," WHERE name = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    $url = $_SESSION['url']; 
                    header("location: $url");
                }
                else
                {
                    echo "FAIL 1";
                }
            }
            else
            {
                echo "ERROR 1";
            }
        }
        else
        {

        }
    }
    else
    {
        echo "ERROR";
    }

}
else 
{
    header('Location: ../index.php');
}
?>