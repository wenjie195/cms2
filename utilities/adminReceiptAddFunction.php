<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Receipt.php';
require_once dirname(__FILE__) . '/../classes/ReceiptDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();
$userUid = $_SESSION['uid'];

function addReceipt($conn,$quotationUid,$quotationSession,$status,$supplierName,$term,$date,$userUid)
{
     if(insertDynamicData($conn,"receipt",array("uid","name","status","bill_to","term","date","user_uid"),
          array($quotationUid,$quotationSession,$status,$supplierName,$term,$date,$userUid),"sssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function addReceiptDetails($conn,$uid,$quotationSession,$quantity,$productName,$unitPrice,$uom,$totalPrice,$status)
{
     if(insertDynamicData($conn,"receipt_details",array("uid","quotation_uid","quantity","product_name","unit_price","uom","total","status"),
          array($uid,$quotationSession,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $quotationUid = md5(uniqid());
     $quotationSession = rewrite($_POST['quotation_session']);

     $supplierName = rewrite($_POST['supplier_name']);
     $term = rewrite($_POST['term']);
     $date = rewrite($_POST['date']);

     $productName = rewrite($_POST['product_name']);
     $quantity = rewrite($_POST['quantity']);
     $unitPrice = rewrite($_POST['unit_price']);
     $uom = rewrite($_POST['uom']);
     // $totalPrice = rewrite($_POST['total_price']);

     $totalPrice = $quantity * $unitPrice;
     $status = "Pending";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productName."<br>";
     // echo $category."<br>";

     if(addReceipt($conn,$quotationUid,$quotationSession,$status,$supplierName,$term,$date,$userUid))
     {
          if(addReceiptDetails($conn,$uid,$quotationSession,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status))
          {
               $_SESSION['receipt'] = $quotationSession;
               header('Location: ../adminReceipt2Add.php');
          }
          else
          {
               echo "fail 1";
          }
     }
     else
     {
          echo "fail 2";
     }
}
else 
{
     header('Location: ../index.php');
}
?>