<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/QuotationDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

// function addQuotation($conn,$quotationUid,$quotationSession,$status)
// {
//      if(insertDynamicData($conn,"quotation",array("uid","name","status"),
//           array($quotationUid,$quotationSession,$status),"sss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function addQuotationDetails($conn,$uid,$quotationSession,$quantity,$productName,$unitPrice,$uom,$totalPrice,$status)
{
     if(insertDynamicData($conn,"quotation_details",array("uid","quotation_uid","quantity","product_name","unit_price","uom","total","status"),
          array($uid,$quotationSession,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $quotationUid = md5(uniqid());
     $quotationSession = rewrite($_POST['quotation_session']);

     // $_SESSION['quotation'] = $quotationSession;

     $productName = rewrite($_POST['product_name']);
     $quantity = rewrite($_POST['quantity']);
     $unitPrice = rewrite($_POST['unit_price']);
     $uom = rewrite($_POST['uom']);
     // $totalPrice = rewrite($_POST['total_price']);

     $totalPrice = $quantity * $unitPrice;
     $status = "Pending";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productName."<br>";
     // echo $category."<br>";

     if(addQuotationDetails($conn,$uid,$quotationSession,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status))
     {
          // $_SESSION['quotation'] = $quotationSession;
          // header('Location: ../adminQuotation2Add.php');
          header('Location: ../marketingQuotation2Add.php');
     }
     else
     {
          echo "fail 1";
     }
}
else 
{
     header('Location: ../index.php');
}
?>