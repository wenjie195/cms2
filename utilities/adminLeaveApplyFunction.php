<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/LeaveStatus.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function addLeave($conn,$uid,$userUid,$fullname,$fromDate,$endDate,$totalDays,$reason,$imageOne,$status)
{
     if(insertDynamicData($conn,"leave_status",array("uid","user_uid","name","start_date","end_date","total_days","reason","doc_one","status"),
          array($uid,$userUid,$fullname,$fromDate,$endDate,$totalDays,$reason,$imageOne,$status),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $fullname = rewrite($_POST['fullname']);

     $userDetails = getUser($conn,"WHERE fullname = ? ", array("fullname") ,array($fullname),"s");
     $userUid = $userDetails[0]->getUid();
     // $totalLeave = $userDetails[0]->getLeaveTotal();
     // $totalLeaveApplied = $userDetails[0]->getLeaveApplied();
     // $leaveRemain = $totalLeave - $totalLeaveApplied;

     $totalDays = rewrite($_POST['total_days']);
     $fromDate = rewrite($_POST['fromDate']);
     $endDate = rewrite($_POST['endDate']);
     $reason = rewrite($_POST['reason']);
     $status = "Pending";

     $newFileOne = $_FILES['image_one']['name'];
     if($newFileOne == '')
     {
         $imageOne = NULL;
     }
     else
     {
          $imageOne = $timestamp.$_FILES['image_one']['name'];
          $target_dir = "../leaveDoc/";
          $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");
          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
          }
     }

     // $imageOne = $timestamp.$_FILES['image_one']['name'];
     // $target_dir = "../leaveDoc/";
     // $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // // Select file type
     // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // // Valid file extensions
     // $extensions_arr = array("jpg","jpeg","png","gif");
     // if( in_array($imageFileType,$extensions_arr) )
     // {
     //      move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     // }

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productName."<br>";
     // echo $category."<br>";

     if(addLeave($conn,$uid,$userUid,$fullname,$fromDate,$endDate,$totalDays,$reason,$imageOne,$status))
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminLeaveAll.php?type=1');
     }
     else
     {
          // echo "fail";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminLeaveApply.php?type=3');
     }

     // if($leaveRemain > $totalDays)
     // {
     //      // if(addLeave($uid,$userUid,$fullname,$fromDate,$endDate,$totalDays,$reason,$imageOne,$status))
     //      if(addLeave($conn,$uid,$userUid,$fullname,$fromDate,$endDate,$totalDays,$reason,$imageOne,$status))
     //      {
     //           $_SESSION['messageType'] = 1;
     //           header('Location: ../adminLeaveAll.php?type=1');
     //      }
     //      else
     //      {
     //           // echo "fail";
     //           $_SESSION['messageType'] = 1;
     //           header('Location: ../adminLeaveApply.php?type=3');
     //      }
     // }
     // else
     // {
     //      $_SESSION['messageType'] = 1;
     //      header('Location: ../adminLeaveApply.php?type=2');
     // }
}
else 
{
     header('Location: ../index.php');
}
?>