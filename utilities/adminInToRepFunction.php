<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Invoice.php';
require_once dirname(__FILE__) . '/../classes/InvoiceDetails.php';
require_once dirname(__FILE__) . '/../classes/Receipt.php';
require_once dirname(__FILE__) . '/../classes/ReceiptDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

function addReceipt($conn,$quotationUid,$quotationName,$status,$billTo,$term,$date,$amount)
{
     if(insertDynamicData($conn,"receipt",array("uid","name","status","bill_to","term","date","amount"),
          array($quotationUid,$quotationName,$status,$billTo,$term,$date,$amount),"sssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function addReceiptDetails($conn,$quoUid,$uid,$quantity,$productName,$unitPrice,$uom,$totalPrice,$status)
{
     if(insertDynamicData($conn,"receipt_details",array("quotation_uid","uid","quantity","product_name","unit_price","uom","total","status"),
          array($quoUid,$uid,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo $quotationName = rewrite($_POST['quotation_uid']);
     echo "<br>";

     $quotationRows = getInvoice($conn, " WHERE name = ? ", array("name") ,array($quotationName),"s");
     $quotationUid = $quotationRows[0]->getUid();
     $billTo = $quotationRows[0]->getBillTo();
     $term = $quotationRows[0]->getTerm();
     $amount = $quotationRows[0]->getAmount();
     $date = $quotationRows[0]->getDate();

     $quotationStatus = "Convert";
     $status = "Pending";

     if(addReceipt($conn,$quotationUid,$quotationName,$status,$billTo,$term,$date,$amount))
     {
          echo "success 1";
          echo "<br>";

          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($quotationStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$quotationStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$quotationUid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"invoice"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               $quotationDetailsRows = getInvoiceDetails($conn, " WHERE quotation_uid = ? ", array("quotation_uid") ,array($quotationName),"s");
               if($quotationDetailsRows)
               {   
                    for ($cnt=0; $cnt < count($quotationDetailsRows) ; $cnt++)
                    {
                         $quoUid = $quotationDetailsRows[$cnt]->getQuotationUid();
                         $uid = $quotationDetailsRows[$cnt]->getUid();
                         $productName = $quotationDetailsRows[$cnt]->getProductName();
                         $quantity = $quotationDetailsRows[$cnt]->getQuantity();
                         $unitPrice = $quotationDetailsRows[$cnt]->getUnitPrice();
                         $uom = $quotationDetailsRows[$cnt]->getUom();
                         $totalPrice = $quotationDetailsRows[$cnt]->getTotal();
     
                         if(addReceiptDetails($conn,$quoUid,$uid,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status))
                         {
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              if($quotationStatus)
                              {
                                   array_push($tableName,"status");
                                   array_push($tableValue,$quotationStatus);
                                   $stringType .=  "s";
                              }
                              array_push($tableValue,$quoUid);
                              $stringType .=  "s";
                              $passwordUpdated = updateDynamicData($conn,"invoice_details"," WHERE quotation_uid = ? ",$tableName,$tableValue,$stringType);
                              if($passwordUpdated)
                              {
                                   // echo "SUCCESS, WOOHOO";
                                   header('Location: ../adminReceiptAll.php');
                              }
                              else
                              {
                                   echo "FAIL GG";
                              }
                         }
                         else
                         {
                              echo "fail 1";
                         }
                    }
               }
          }
          else
          {
               echo "fail (update quotation status)";
          }
     }
     else
     {
          echo "fail 2";
     }
}
else 
{
     header('Location: ../index.php');
}
?>