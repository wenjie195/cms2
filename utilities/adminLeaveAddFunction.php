<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/LeaveStatus.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $userUid = rewrite($_POST['user_uid']);

     $userDetails = getUser($conn," WHERE uid = ? ", array("uid") ,array($userUid),"s");
     // $userUid = $userDetails[0]->getUid();
     $totalLeave = $userDetails[0]->getLeaveTotal();
     $renewLeave = $totalLeave + 1 ;

    if($userDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($renewLeave)
        {
            array_push($tableName,"leave_total");
            array_push($tableValue,$renewLeave);
            $stringType .=  "s";
        }
        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminDashboard.php?type=3');
        }
        else
        {
            echo "ERROR 2";
        }
    }
    else
    {
        echo "ERROR 3";
    }

}
else 
{
     header('Location: ../index.php');
}
?>