<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/Invoice.php';
// require_once dirname(__FILE__) . '/../classes/InvoiceDetails.php';
require_once dirname(__FILE__) . '/../classes/Receipt.php';
require_once dirname(__FILE__) . '/../classes/ReceiptDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo $quotationName = rewrite($_POST['quotation_uid']);
     echo "<br>";

     $quotationRows = getReceipt($conn, " WHERE name = ? ", array("name") ,array($quotationName),"s");
     // echo $quotationUid = $quotationRows[0]->getUid();
     // echo "<br>";

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz));
     $dt->setTimestamp($timestamp);
     // echo $currentMonth = $dt->format('m');
     echo $currentMonth = $dt->format('M');
     echo "<br>";
     echo $currentYear = $dt->format('Y');
     echo "<br>";

     $quotationStatus = "Closed";
     // $status = "Pending";

     if($quotationRows)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($quotationStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$quotationStatus);
               $stringType .=  "s";
          }

          if($currentMonth)
          {
               array_push($tableName,"month");
               array_push($tableValue,$currentMonth);
               $stringType .=  "s";
          }
          if($currentYear)
          {
               array_push($tableName,"year");
               array_push($tableValue,$currentYear);
               $stringType .=  "s";
          }

          array_push($tableValue,$quotationName);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"receipt"," WHERE name = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($quotationStatus)
               {
                    array_push($tableName,"status");
                    array_push($tableValue,$quotationStatus);
                    $stringType .=  "s";
               }
               array_push($tableValue,$quotationName);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"receipt_details"," WHERE quotation_uid = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    // echo "SUCCESS, WOOHOO";
                    header('Location: ../adminReceiptAll.php');
               }
               else
               {
                    echo "FAIL GG";
               }
          }
          else
          {
               echo "fail (update quotation status)";
          }
     }
     else
     {
          echo "fail 2";
     }
}
else 
{
     header('Location: ../index.php');
}
?>