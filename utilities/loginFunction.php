<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

$conn = connDB();

$conn->close();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        // $email = rewrite($_POST['email']);

        $username = rewrite($_POST['username']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {
                    $_SESSION['uid'] = $user->getUid();
                    // $_SESSION['usertype_level'] = $user->getUserType();
                    $_SESSION['user_type'] = $user->getUserType();
                    
                    if($user->getUserType() == 0)
                    {
                        header('Location: ../adminDashboard.php');
                        // echo "admin page";
                    }
                    if($user->getUserType() == 1)
                    {
                        // echo "normal user";
                        // apply leave only
                        header('Location: ../userDashboard.php');
                    }
                    if($user->getUserType() == 2)
                    {
                       // echo "markerting";
                       header('Location: ../marketingDashboard.php');
                    }
                    if($user->getUserType() == 10)
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=4');
                    }
                    else
                    {
                        echo "unknown user type";
                    }
                }
                else 
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=3');
                    // promptError("Incorrect email or password");
                    // echo "incorrect email or password";
                    // echo "<script>alert('incorrect password');window.location='../index.php'</script>";
                }

        }
        else
        {
        //   $_SESSION['messageType'] = 1;
        //   header('Location: ../index.php?type=7');
        //   echo "no user with this email ";
          echo "<script>alert('no user with this username');window.location='../index.php'</script>";
          //   promptError("This account does not exist");
        }
    }

    $conn->close();
}
?>