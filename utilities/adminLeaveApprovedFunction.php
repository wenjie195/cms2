<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/LeaveStatus.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $itemUid = rewrite($_POST["leave_uid"]);
    $userUid = rewrite($_POST["user_uid"]);

    $updateStatus = "Approved";

    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($userUid),"s");
    $leaveApplied = $userDetails[0]->getLeaveApplied();

    $leaveDetails = getLeaveStatus($conn," WHERE uid = ? ",array("uid"),array($itemUid),"s");    
    $totalDays = $leaveDetails[0]->getTotalDays();

    $totalLeaveApplied = $totalDays + $leaveApplied;

    if($leaveDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($updateStatus)
        {
            array_push($tableName,"status");
            array_push($tableValue,$updateStatus);
            $stringType .=  "s";
        }
        array_push($tableValue,$itemUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"leave_status"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            if($totalLeaveApplied)
            {
                array_push($tableName,"leave_applied");
                array_push($tableValue,$totalLeaveApplied);
                $stringType .=  "s";
            }
            array_push($tableValue,$userUid);
            $stringType .=  "s";
            $userUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($userUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminLeaveAll.php?type=4');
            }
            else
            {
                echo "ERROR 1";
            }

        }
        else
        {
            echo "ERROR 2";
        }
    }
    else
    {
        echo "ERROR 3";
    }

}
else 
{
    header('Location: ../index.php');
}
?>