<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Quotation.php';
require_once dirname(__FILE__) . '/../classes/QuotationDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());
     // $quotationUid = md5(uniqid());
     // $quotationSession = rewrite($_POST['quotation_session']);
     echo $quotationSession = rewrite($_POST['quotation_session']);
     echo "<br>";

     $quotationDetails = getQuotationDetails($conn, " WHERE quotation_uid = ? ", array("quotation_uid") ,array($quotationSession),"s");
     if ($quotationDetails)
     {
         $totalAmount = 0; // initital
         for ($b=0; $b <count($quotationDetails) ; $b++)
         {
             $totalAmount += $quotationDetails[$b]->getTotal();
         }
     }
     echo $totalAmount;
     echo "<br>";

     if($quotationDetails)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($totalAmount)
          {
               array_push($tableName,"amount");
               array_push($tableValue,$totalAmount);
               $stringType .=  "s";
          }
          array_push($tableValue,$quotationSession);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"quotation"," WHERE name = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               // echo "SUCCESS";
               unset($_SESSION['quotation_session']);
               header('Location: ../marketingQuotationAll.php');
          }
          else
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../adminViewMember.php?type=4');
               echo "FAIL";
          }
     }
     else
     {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../adminViewMember.php?type=5');
          echo "ERROR";
     }
}
else 
{
     header('Location: ../index.php');
}
?>