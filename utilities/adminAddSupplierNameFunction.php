<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Name.php';
// require_once dirname(__FILE__) . '/../classes/Category.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function addCategory($conn,$uid,$supplierName,$status)
{
     if(insertDynamicData($conn,"name",array("uid","name","status"),
          array($uid,$supplierName,$status),"sss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $supplierName = rewrite($_POST['supplier_name']);
     $status = "Available";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";

     $allName = getName($conn," WHERE name = ? ",array("name"),array($_POST['supplier_name']),"s");
     $existingName = $allName[0];

     if (!$existingName)
     {
          if(addCategory($conn,$uid,$supplierName,$status))
          {
               header('Location: ../adminAddSupplierName.php');
          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "supplier name existed !! pls recheck";
     }
}
else 
{
     header('Location: ../index.php');
}
?>