<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Quotation.php';
require_once dirname(__FILE__) . '/../classes/QuotationDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

function addQuotationDetails($conn,$uid,$quotationSession,$quantity,$productName,$unitPrice,$uom,$totalPrice,$status)
{
     if(insertDynamicData($conn,"quotation_details",array("uid","quotation_uid","quantity","product_name","unit_price","uom","total","status"),
          array($uid,$quotationSession,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status),"ssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     // $quotationUid = md5(uniqid());
     $quotationSession = rewrite($_POST['quotation_session']);

     // $_SESSION['quotation'] = $quotationSession;

     $productName = rewrite($_POST['product_name']);
     $quantity = rewrite($_POST['quantity']);
     $unitPrice = rewrite($_POST['unit_price']);
     $uom = rewrite($_POST['uom']);
     // $totalPrice = rewrite($_POST['total_price']);

     $totalPrice = $quantity * $unitPrice;
     $status = "Pending";

     //   FOR DEBUGGING 
     echo "<br>";
     echo $quotationSession."<br>";
     // echo $category."<br>";

     if(addQuotationDetails($conn,$uid,$quotationSession,$productName,$quantity,$unitPrice,$uom,$totalPrice,$status))
     {
          // $updateQuotation = getQuotationDetails($conn," WHERE quotation_uid = ? ",array("quotation_uid"),array($quotationSession),"s");    
          // $updateQuotation = getQuotationDetails($conn," WHERE quotation_uid = ? AND status != 'Delete' ",array("quotation_uid"),array($quotationName),"s");   
          $updateQuotation = getQuotationDetails($conn," WHERE quotation_uid = ? AND status != 'Delete' ",array("quotation_uid"),array($quotationSession),"s");   
          if ($updateQuotation)
          {
               $totalAmount = 0; // initital
               for ($b=0; $b <count($updateQuotation) ; $b++)
               {
                    $totalAmount += $updateQuotation[$b]->getTotal();
               }
          }

          if($updateQuotation)
          {   
              $tableName = array();
              $tableValue =  array();
              $stringType =  "";
              //echo "save to database";
      
               if($totalAmount)
               {
                    array_push($tableName,"amount");
                    array_push($tableValue,$totalAmount);
                    $stringType .=  "s";
               }        
               array_push($tableValue,$quotationSession);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"quotation"," WHERE name = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    header('Location: ../adminQuotationAll.php');
               }
               else
               {
                    echo "FAIL 1";
               }
          }
          else
          {
              echo "ERROR 1";
          }
     }
     else
     {
          echo "fail 1";
     }
}
else 
{
     header('Location: ../index.php');
}
?>