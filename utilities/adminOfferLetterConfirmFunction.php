<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Offer.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    // $uid = $_SESSION['uid'];
    $letterUid = rewrite($_POST['letter_uid']);
    // $date = rewrite($_POST['date']);
    $status = "Confirm";
  
    $letterDetails = getOffer($conn," WHERE uid = ? ",array("uid"),array($letterUid),"s");
    // $icDetails = $icRows[0];

    if ($letterDetails)
    {
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        array_push($tableValue,$letterUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"offer"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            header('Location: ../adminLetter.php');
        }
        else
        {
            echo "<script>alert('FAIL !!');window.location='../adminLetter.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminLetter.php'</script>";
    }
}
else 
{
    header('Location: ../index.php');
}
?>
