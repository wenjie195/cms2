<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $month = rewrite($_POST["month"]);
    $years = rewrite($_POST["years"]);
}
else 
{   }

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Sales Details | CMS" />
    <title>Sales Details | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

  <h1 class="h1-title open">Sales Details</h1>

  <div class="big-four-input-container">
    <!-- <form method='post' action='adminStaffPSSessionSearch.php'>  -->
    <form action="utilities/adminSalesGenerateFunction.php" method="POST">

      <div class="big-four-input-container">
        <div class="input50-div">
          <p class="input-top-p">Rental</p>
          <input type="number" placeholder="Rental" name="rental" id="rental" class="tele-four-input tele-input clean" required>
        </div>

        <div class="input50-div second-input50">
          <p class="input-top-p">Other Expenses</p>
          <input type="text" placeholder="Other Expenses" name="expenses" id="expenses" class="tele-four-input tele-input clean" required>
        </div>
      </div>

      <input type="hidden" value="<?php echo $month;?>" name="month" id="month" class="tele-four-input tele-input clean" readonly>
      <input type="hidden" value="<?php echo $years;?>" name="year" id="year" class="tele-four-input tele-input clean" readonly>

      <div class="clear"></div>

     
      <button class="clean blue-btn red-btn margin-top30 fix300-btn margin-bottom30" name="submit">Complete</button>
     <div class="clear"></div>


    </form>         
  </div>

</div>

<style>
.sales-li{
	color:#264a9c;
	background-color:white;}
.sales-li .hover1a{
	display:none;}
.sales-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>