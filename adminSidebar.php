<div class="side-bar red-bg no-print">
	<div class="width100 text-center">
    	<!--<img src="img/gp.png" class="logo-img" alt="logo" title="logo">-->
    </div>
	<ul class="sidebar-ul">
    	<a href="adminDashboard.php">
        	<li class="sidebar-li dashboard-li hover1">
            	<img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                 <p>Dashboard</p>
            </li>
        </a>

        <a href="adminLetter.php">
        	<li class="sidebar-li offer-li hover1">
            	<img src="img/letter3.png" class="hover1a" alt="Letter" title="Letter">
                <img src="img/letter.png" class="hover1b" alt="Letter" title="Letter">
                 <p>Letter</p>
            </li>
        </a>

    	<a href="adminStaffAdd.php">
        	<li class="sidebar-li account-li hover1">
            	<img src="img/create-account1.png" class="hover1a" alt="Add Staff" title="Add Staff">
                <img src="img/create-account2.png" class="hover1b" alt="Add Staff" title="Add Staff">
                 <p>Add Staff</p>
            </li>
        </a>

        <a href="adminStaffPS.php">
        	<li class="sidebar-li payslip-li hover1">
            	<img src="img/salary0.png" class="hover1a" alt="Payslip/Salary Details" title="Payslip/Salary Details">
                <img src="img/salary1.png" class="hover1b" alt="Payslip/Salary Details" title="Payslip/Salary Details">
                 <p>Payslip/Salary Details</p>
            </li>
        </a>
    
        <a href="adminLeaveAll.php">
        	<li class="sidebar-li leave-li hover1">
            	<img src="img/leave.png" class="hover1a" alt="Leave" title="Leave">
                <img src="img/leave2.png" class="hover1b" alt="Leave" title="Leave ">
                 <p>Leave</p>
            </li>
        </a>

        <a href="adminQuotationAll.php">
        	<li class="sidebar-li quotation-li hover1">
            	<img src="img/quotation.png" class="hover1a" alt="Quotation" title="Quotation">
                <img src="img/quotation2.png" class="hover1b" alt="Quotation" title="Quotation">
                 <p>Quotation</p>
            </li>
        </a>

        <a href="adminInvoiceAll.php">
        	<li class="sidebar-li invoice-li hover1">
            	<img src="img/invoicea.png" class="hover1a" alt="Invoice" title="Invoice">
                <img src="img/invoicea1.png" class="hover1b" alt="Invoice" title="Invoice">
                 <p>Invoice</p>
            </li>
        </a>

        <a href="adminReceiptAll.php">
        	<li class="sidebar-li receipt-li hover1">
            	<img src="img/invoice.png" class="hover1a" alt="Receipt" title="Receipt">
                <img src="img/invoice2.png" class="hover1b" alt="Receipt" title="Receipt">
                 <p>Receipt</p>
            </li>
        </a>

        <a href="adminSalesReport.php">
        	<li class="sidebar-li sales-li hover1">
            	<img src="img/sales3.png" class="hover1a" alt="Sales Report" title="Sales Report">
                <img src="img/sales1.png" class="hover1b" alt="Sales Report" title="Sales Report">
                 <p>Company Sales Report</p>
            </li>
        </a>

        <a href="logout.php">
        	<li class="sidebar-li hover1">
            	<img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
            	 <p>Logout</p>
            </li>
       </a>
    </ul>
</div>

<header id="header" class="header header--fixed same-padding header1 menu-color no-print" role="banner">
        <img src="img/gp-white.png" class="mobile-logo" alt="Logo" title="Logo">
        <div id="dl-menu" class="dl-menuwrapper">
            <button class="dl-trigger">Open Menu</button>
            <ul class="dl-menu">
   	<a href="adminDashboard.php">
        	<li class="sidebar-li dashboard-li hover1">
            	<img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                 <p>Dashboard</p>
            </li>
        </a>

        <a href="adminLetter.php">
        	<li class="sidebar-li offer-li hover1">
            	<img src="img/letter3.png" class="hover1a" alt="Letter" title="Letter">
                <img src="img/letter.png" class="hover1b" alt="Letter" title="Letter">
                 <p>Letter</p>
            </li>
        </a>

    	<a href="adminStaffAdd.php">
        	<li class="sidebar-li account-li hover1">
            	<img src="img/create-account1.png" class="hover1a" alt="Add Staff" title="Add Staff">
                <img src="img/create-account2.png" class="hover1b" alt="Add Staff" title="Add Staff">
                 <p>Add Staff</p>
            </li>
        </a>

        <a href="adminStaffPS.php">
        	<li class="sidebar-li payslip-li hover1">
            	<img src="img/salary0.png" class="hover1a" alt="Payslip/Salary Details" title="Payslip/Salary Details">
                <img src="img/salary1.png" class="hover1b" alt="Payslip/Salary Details" title="Payslip/Salary Details">
                 <p>Payslip/Salary Details</p>
            </li>
        </a>
    
        <a href="adminLeaveAll.php">
        	<li class="sidebar-li leave-li hover1">
            	<img src="img/leave.png" class="hover1a" alt="Leave" title="Leave">
                <img src="img/leave2.png" class="hover1b" alt="Leave" title="Leave ">
                 <p>Leave</p>
            </li>
        </a>

        <a href="adminQuotationAll.php">
        	<li class="sidebar-li quotation-li hover1">
            	<img src="img/quotation.png" class="hover1a" alt="Quotation" title="Quotation">
                <img src="img/quotation2.png" class="hover1b" alt="Quotation" title="Quotation">
                 <p>Quotation</p>
            </li>
        </a>

        <a href="adminInvoiceAll.php">
        	<li class="sidebar-li invoice-li hover1">
            	<img src="img/invoicea.png" class="hover1a" alt="Invoice" title="Invoice">
                <img src="img/invoicea1.png" class="hover1b" alt="Invoice" title="Invoice">
                 <p>Invoice</p>
            </li>
        </a>

        <a href="adminReceiptAll.php">
        	<li class="sidebar-li receipt-li hover1">
            	<img src="img/invoice.png" class="hover1a" alt="Receipt" title="Receipt">
                <img src="img/invoice2.png" class="hover1b" alt="Receipt" title="Receipt">
                 <p>Receipt</p>
            </li>
        </a>


        <a href="logout.php">
        	<li class="sidebar-li hover1">
            	<img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
            	 <p>Logout</p>
            </li>
       </a>
            </ul>
    </div>
</header>