<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Edit Staff Password | CMS" />
    <title>Admin Edit Staff Password | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar">

    <!-- <h1 class="h1-title">Edit Company</h1> -->

    <h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Edit Staff Password
        </a>
    </h1>

    <div class="clear"></div>

    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

    <form action="utilities/adminStaffEditPasswordFunction.php" method="POST">

        <div class="input50-div">
        	<p class="input-title-p">New Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="New Password" id="new_password" name="new_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>   
        </div>

        <div class="input50-div second-input50">
        	<p class="input-title-p">Retype New Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
            </div>
        </div>      

        <div class="clear"></div>

        <input type="hidden" value="<?php echo $userDetails[0]->getUid();?>" id="user_uid" name="user_uid" readonly>        

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

    </form>

    <?php
    }
    ?>

</div>


<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

<script>
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>