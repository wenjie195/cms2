<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Quotation.php';
require_once dirname(__FILE__) . '/classes/Offer.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Offer Letter | CMS" />
    <title>Offer Letter | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar page1-div">
	<!-- <p class="invoice-logo-p text-center"><img src="img/invoice-logo.png" class="invoice-logo" alt="ChiNou Smart Solutions Sdn. Bhd." title="ChiNou Smart Solutions Sdn. Bhd."></p> -->
    <div class="text-center header-div">
<!--        <div class="left-header">
            <img src="img/gp.png" class="header-logo" alt="GP7" title="GP7">
        </div>-->
        <div class="right-header">
            <h3 class="invoice-company-name">ABC SDN. BHD.</h3>
            <h3 class="invoice-company-number invoice-company-number2 margin-bottom5">2021231013066(1369386-H)</h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number"></h3>
        </div>
    </div>
    <div class="clear"></div>
    <?php
    // echo $_POST['letter_uid'];
    if(isset($_POST['letter_uid']))
    {
    $conn = connDB();
    $letterDetails = getOffer($conn,"WHERE uid = ? ", array("uid") ,array($_POST['letter_uid']),"s");
    // $orderId = $ordersDetails[0]->getId();
    ?>

        <p class="ms-p name-p"><?php echo $letterDetails[0]->getName();?></p>
        <p class="ms-p ic-p margin-top5"><?php echo $letterDetails[0]->getIcno();?></p>
        <p class="ms-p margin-top5"><?php echo $letterDetails[0]->getAddressOne();?></p>
        <p class="ms-p add-p"><?php echo $letterDetails[0]->getAddressTwo();?></p>
        <p class="ms-p add-p"><?php echo $letterDetails[0]->getAddressThree();?></p>
        <p class="ms-p add-p"><?php echo $letterDetails[0]->getAddressFour();?></p>

        <div class="clear"></div>

        <p class="ms-p right-date-p"><?php echo $letterDetails[0]->getDate();?></p>

        <div class="clear"></div>

        <p class="ms-p"><b><u>Letter of Offer</u></b></p>

       

            <p class="ms-p margin-top20">Dear [MS/MR],</p>

           

            <p class="ms-p margin-top20">With great pleasure, I extend the following employment offer to you.</p>

           

            <p class="ms-p margin-top20">Position: <?php echo $letterDetails[0]->getPosition();?></p>
            <p class="ms-p margin-top5">Start date: <?php echo $letterDetails[0]->getStartDate();?></p>
            <p class="ms-p margin-top5">Working hours: <?php echo $letterDetails[0]->getWorkingHrs();?></p>
            <p class="ms-p margin-top5">Salary: RM<?php echo $letterDetails[0]->getSalary();?></p>
            <p class="ms-p margin-top5">Probation period: <?php echo $letterDetails[0]->getProbation();?></p>

            <p class="ms-p margin-top20">In this position, you will be responsible to  <?php echo $letterDetails[0]->getJobScope();?>  and other related jobs for the company.</p>

            
            
            <p class="ms-p margin-top20">Your employment with the company will be on mutual agreement basis, which means you and the company are free to terminate the employment relationship at any time for any reason during the probation period. This letter is not a contract or guarantee of employment for definite amount of time.</p>
           
            <p class="ms-p margin-top20">Please confirm your acceptance of this offer by signing and return this letter.</p>

            

            <p class="ms-p margin-top20">We are excited to have you join our team! If you have any questions, please feel free to reach out at any time. </p>

          

            <p class="ms-p margin-top20">Sincerely,</p>

           

            <p class="ms-p sign-line"></p>
            <p class="ms-p">Director Name: </p>
            <p class="ms-p">Director</p>
            <!--<p class="ms-p">Director</p>-->

            <p class="ms-p left-signature">Signature: </p>
            <p class="ms-p sign-line right-line"></p>
            <div class="clear"></div>
            <p class="ms-p">Candidate Name: </p>
            <p class="ms-p">Date:</p>

            

            <button class="clean red-btn margin-top30 fix300-btn no-print" onclick="window.print()">Generate PDF</button>
			<p class="text-center pointer red-link no-print margin-bottom30"><a href="./img/pdf-tutorial.jpg" data-fancybox class="red-link">Tutorial</a></p>
        

    <?php
    }
    ?>
    
    <div class="clear"></div>

</div>

<!-- <style>
.invoice-li{
	color:#264a9c;
	background-color:white;}
.invoice-li .hover1a{
	display:none;}
.invoice-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

</body>
</html>