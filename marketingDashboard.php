<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Markerting Dashboard | CMS" />
    <title>Markerting Dashboard | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'userSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="h1-title open">Marketing Dashboard</h1>

    <div class="clear"></div>

    <div class="width100 overflow">
        <div class="four-div white-to-pink-div hover1">
            <img src="img/leave2.png" class="big-icon hover1a" alt="Total Annual Leave" title="Total Annual Leave">
            <img src="img/leave.png" class="big-icon hover1b" alt="Total Annual Leave" title="Total Annual Leave">
            <p class="min-height-p">Total Annual Leave</p>
            <p><?php echo $totalLeave = $userData->getLeaveTotal();?></p>
        </div>

        <div class="four-div left-mid-four white-to-pink-div hover1">
            <img src="img/applied-leave.png" class="big-icon hover1a" alt="Total Annual Leave" title="Total Annual Leave">
            <img src="img/applied-leave2.png" class="big-icon hover1b" alt="Total Annual Leave" title="Total Annual Leave">
            <p class="min-height-p">Annual Leave (Applied)</p>
            <p><?php echo $leaveApplied = $userData->getLeaveApplied();?></p>
            <!-- <p>00</p> -->
        </div>

        <div class="tempo-2-clear"></div>

        <div class="four-div right-mid-four white-to-pink-div hover1">
            <img src="img/remaining-leave.png" class="big-icon hover1a" alt="Total Annual Leave" title="Total Annual Leave">
            <img src="img/remaining-leave2.png" class="big-icon hover1b" alt="Total Annual Leave" title="Total Annual Leave">
            <p class="min-height-p">Annual Leave (Remain)</p>
            <p><?php echo $leaveRemain = $totalLeave - $leaveApplied;?></p>
        </div>

        <!-- <div class="four-div white-to-pink-div hover1">
            <img src="img/import-data-big.png" class="big-icon hover1a"  alt="Import Data" title="Import Data">
            <img src="img/import-data-big2.png" class="big-icon hover1b"  alt="Import Data" title="Import Data">
            <p>Import Data</p>
        </div>   -->
	</div>

    <div class="clear"></div>

</div>

<style>
.dashboard-li{
	color:#264a9c;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Staff Added !"; 
        }
        // elseif($_GET['type'] == 2)
        // {
        //     $messageType = "Please Register"; 
        // }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Incorrect Password";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>