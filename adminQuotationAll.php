<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/Quotation.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getQuotation($conn);
$productDetails = getQuotation($conn, " ORDER BY date_created DESC ");
// $productDetails = getQuotation($conn, "WHERE status = 'Pending' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="All Quotation | CMS" />
    <title>All Quotation | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <!-- <h1 class="h1-title open">Quotation (Pending)</h1> -->
    <h1 class="h1-title open">Quotation</h1>

    <a href='adminQuotationPre.php'><div class="blue-btn width195">Issue A New Quotation</div></a>

    <div class="clear"></div>

    <?php
      $totalAmount = 0; // initital
      for ($b=0; $b <count($productDetails) ; $b++)
      {
          $totalAmount += $productDetails[$b]->getAmount();
      }
    ?>

    <h4 class="input-title-p">Total Amount : RM <?php echo $totalAmount;?></h4>

    <div class="clear"></div>

    <div class="width100 shipping-div2 margin-top40">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Quotation Name</th>
                    <th>Amount</th>                    
                    <th>Status</th>
                    <th>Date</th>
                    <th>Details</th>
                    <th>Action</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getName();?></td>
                            <td><?php echo $productDetails[$cnt]->getAmount();?></td>
                            <td><?php echo $productDetails[$cnt]->getStatus();?></td>
                            <td><?php echo $productDetails[$cnt]->getDateCreated();?></td>

                            <td>
                              <form action="adminQuotationView.php" method="POST">
                                <button class="clean hover1 img-btn" type="submit" name="quotation_uid" value="<?php echo $productDetails[$cnt]->getName();?>">
                                  <img src="img/view.png" class="width100 hover1a" alt="View" title="View">
                                  <img src="img/view2.png" class="width100 hover1b" alt="View" title="View">
                                </button>
                              </form>
                            </td>

                            <td>
                              <?php 
                                $quotationStatus = $productDetails[$cnt]->getStatus();
                                if($quotationStatus == 'Pending')
                                {
                                ?>
                                  <a href='adminQuotationEdit.php?id=<?php echo $productDetails[$cnt]->getName();?>'>
                                    <button class="clean hover1 img-btn" type="submit" name="quotation_uid" value="<?php echo $productDetails[$cnt]->getName();?>">
                                      <img src="img/edit2.png" class="width100 hover1a" alt="Edit" title="Edit">
                                      <img src="img/edit.png" class="width100 hover1b" alt="Edit" title="Edit">
                                    </button>
                                  </a>
                                <?php
                                }
                                else
                                {
                                  echo "Converted To Invoice";
                                }
                              ?>
                            </td>

                            <td>
                              <?php 
                                $status = $productDetails[$cnt]->getStatus();
                                if($status == 'Convert')
                                {
                                  echo "Converted To Invoice";
                                }
                                else
                                {
                                ?>
                                  <form action="utilities/adminQuoToInFunction.php" method="POST">
                                    <button class="clean blue-btn" type="submit" name="quotation_uid" value="<?php echo $productDetails[$cnt]->getName();?>">
                                      Transfer To Invoice
                                    </button>
                                  </form>
                                <?php
                                }
                              ?>
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.quotation-li{
	color:#264a9c;
	background-color:white;}
.quotation-li .hover1a{
	display:none;}
.quotation-li .hover1b{
	display:block;}
</style>

<?php unset($_SESSION['quotation_session']); unset($_SESSION['url']);?>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Staff Added !"; 
        }
        // elseif($_GET['type'] == 2)
        // {
        //     $messageType = "Please Register"; 
        // }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Incorrect Password";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>