<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Quotation.php';
require_once dirname(__FILE__) . '/classes/QuotationDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$_SESSION['url'] = $_SERVER['REQUEST_URI'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Edit Quotation | CMS" />
    <title>Edit Quotation | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'userSidebar.php'; ?>

<div class="width100 next-to-sidebar padding-bottom50">
   
    <h1 class="h1-title">Edit Quotation</h1> 

    <?php
    // echo $_POST['quotation_uid'];
    // if(isset($_POST['quotation_uid']))
    // echo $_GET['id'];
    if(isset($_GET['id']))
    {
    $conn = connDB();
    // $quotation = getQuotation($conn,"WHERE name = ? ", array("name") ,array($_POST['quotation_uid']),"s");
    // $quotationDetails = getQuotationDetails($conn,"WHERE quotation_uid = ? ", array("quotation_uid") ,array($_POST['quotation_uid']),"s");
    $quotation = getQuotation($conn,"WHERE name = ? ", array("name") ,array($_GET['id']),"s");
    // $quotationDetails = getQuotationDetails($conn,"WHERE quotation_uid = ? ", array("quotation_uid") ,array($_GET['id']),"s");
    $quotationDetails = getQuotationDetails($conn,"WHERE quotation_uid = ? AND status != 'Delete' ", array("quotation_uid") ,array($_GET['id']),"s");
    // $orderId = $ordersDetails[0]->getId();
    ?>

        <!-- <a href='adminQuotationAddExtra.php?id=<?php echo $_GET['id'];?>'> -->
        <a href='marketingQuotationAddExtra.php?id=<?php echo $_GET['id'];?>'>
            <div class="blue-btn width195">Add Extra Item</div>
        </a>

        <!-- <div class="right-invoice-div"> -->
        <div class="left-invoice-div">
            <!-- <p class="ms-p">Quotation Name: <b><?php //echo $_POST['quotation_uid'];?></b></p> -->
            <p class="ms-p">Quotation Name: <b><?php echo $_GET['id'];?></b></p>
		</div>

        <div class="clear"></div>

        <div class="width100" style="overflow-x:auto;">

            <table class="shipping-table print-table" id="myTable">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Description</th>
                        <th style="white-space:nowrap">Qty &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                        <th>UOM</th>
                        <th>U/Price (RM)</th>
                        <th>Total (RM)</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // $conn = connDB();
                    if($quotationDetails)
                    {   
                        for($cnt = 0;$cnt < count($quotationDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $quotationDetails[$cnt]->getProductName();?></td>

                                <td>
                                    <form method="POST" action="utilities/quotationSubtractFunction.php" class="oz-form">
                                        <input class="clean" type="hidden" value="<?php echo $quotationDetails[$cnt]->getUnitPrice();?>" id="unit_price" name="unit_price" readonly>
                                        <input class="clean" type="hidden" value="<?php echo $quotationDetails[$cnt]->getQuantity();?>" id="quantity" name="quantity" readonly>
                                        <input class="clean" type="hidden" value="<?php echo $quotationDetails[$cnt]->getQuotationUid();?>" id="quotation_name" name="quotation_name" readonly>
                                        <button class="clean left-minus oz-btn" type="submit" name="item_uid" value="<?php echo $quotationDetails[$cnt]->getUid();?>">
                                            -
                                        </button>
                                    </form>

                                    <p class="oz-p"><?php echo $quotationDetails[$cnt]->getQuantity();?></p>

                                    <form method="POST" action="utilities/quotationAddFunction.php" class="oz-form">
                                        <input class="clean" type="hidden" value="<?php echo $quotationDetails[$cnt]->getUnitPrice();?>" id="unit_price" name="unit_price" readonly>
                                        <input class="clean" type="hidden" value="<?php echo $quotationDetails[$cnt]->getQuantity();?>" id="quantity" name="quantity" readonly>
                                        <input class="clean" type="hidden" value="<?php echo $quotationDetails[$cnt]->getQuotationUid();?>" id="quotation_name" name="quotation_name" readonly>
                                        <button class="clean left-minus oz-btn" type="submit" name="item_uid" value="<?php echo $quotationDetails[$cnt]->getUid();?>">
                                            +
                                        </button>
                                    </form>
                                </td>
                                
                                <td><?php echo $quotationDetails[$cnt]->getUom();?></td>
                                <td><?php echo $quotationDetails[$cnt]->getUnitPrice();?></td>
                                <td><?php echo $quotationDetails[$cnt]->getTotal();?></td>

                                <td>
                                    <form method="POST" action="utilities/quotationDeleteFunction.php" class="oz-form">
                                        <input class="clean" type="hidden" value="<?php echo $quotationDetails[$cnt]->getQuotationUid();?>" id="quotation_name" name="quotation_name" readonly>
                                        <button class="clean hover1 img-btn" type="submit" name="item_uid" value="<?php echo $quotationDetails[$cnt]->getUid();?>">
                                            <img src="img/close.png" class="width100 hover1a" alt="Delete" title="Delete">
                                            <img src="img/close2.png" class="width100 hover1b" alt="Delete" title="Delete">
                                        </button>
                                    </form>
                                </td>

                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    $conn->close();
                    ?>
                 <tr>
                    <td colspan="5">Grand Total:</td>
                    <td><b><?php echo $quotation[0]->getAmount();?></b></td>
                </tr>
                </tbody>
            </table>

        </div>

    <?php
    }
    ?>
    
    <div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>