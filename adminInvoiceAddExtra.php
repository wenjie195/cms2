<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Quotation.php';
// require_once dirname(__FILE__) . '/classes/QuotationDetails.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';
require_once dirname(__FILE__) . '/classes/InvoiceDetails.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Invoice (Add Extra Item) | CMS" />
    <title>Invoice (Add Extra Item) | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Add Extra Item (Invoice)</h1> 

    <!-- <form action="utilities/adminQuotationAddExtraFunction.php" method="POST"> -->
    <form action="utilities/adminInvoiceAddExtraFunction.php" method="POST">

        <?php
        $_GET['id'];
        {
        ?>
        <?php
        }
        ?>

        <h5 class="h1-title">
            Your Session Name : <?php echo $_GET['id'];?>
        </h5> 

        <input class="clean tele-input" type="hidden" value="<?php echo $_GET['id'];?>" id="quotation_session" name="quotation_session" readonly>       

        <div class="input50-div">
            <p class="input-title-p">Product Name</p>
            <input class="clean tele-input" type="text" placeholder="Product Name" id="product_name" name="product_name" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Quantity</p>
            <input class="clean tele-input"  type="number" placeholder="Quantity" id="quantity" name="quantity" required>        
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Unit Price (RM)</p>     
            <input class="clean tele-input"  type="text" placeholder="Unit Price" id="unit_price" name="unit_price" required> 
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">UOM</p>
            <input class="clean tele-input"  type="text" placeholder="UOM" id="uom" name="uom">        
        </div> 

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

        <div class="clear"></div>
    </form>
</div>

<?php include 'js.php'; ?>

</body>
</html>