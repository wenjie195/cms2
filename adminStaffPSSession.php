<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Name.php';
// require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Payslip Details | CMS" />
    <title>Payslip Details | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Payslip Details</h1> 

    <!-- <a href='uploadExcel.php'><div class="blue-btn width175">Issue Payslip</div></a> -->
    <form action="uploadExcel.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Month</p>
            <select class="clean tele-input" type="text" id="month" name="month" required>
                <option value="" name=" ">PLEASE SELECT MONTH</option>
                <option value="Jan" name="Jan">Jan</option>
                <option value="Feb" name="Feb">Feb</option>
                <option value="Mar" name="Mar">Mar</option>
                <option value="Apr" name="Apr">Apr</option>
                <option value="May" name="May">May</option>
                <option value="Jun" name="Jun">Jun</option>
                <option value="Jul" name="Jul">Jul</option>
                <option value="Aug" name="Aug">Aug</option>
                <option value="Sep" name="Sep">Sep</option>
                <option value="Oct" name="Oct">Oct</option>
                <option value="Nov" name="Nov">Nov</option>
                <option value="Dec" name="Dec">Dec</option>
            </select>
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Years (eg. 2021)</p>
            <input type="text" placeholder="Years" class="clean tele-input" name='years' id="years" required>
        </div> 

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn margin-bottom" name="submit">Continue</button>

        <div class="clear"></div>
    </form>
</div>

<?php include 'js.php'; ?>

</body>
</html>