<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$month = rewrite($_POST["month"]);
	$years = rewrite($_POST["years"]);

  $productDetails = getPayment($conn, " WHERE fullname != 'Fullname' AND month = '$month' AND year = '$years' ORDER BY date_created DESC ");
  $totalEpf = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalEpf += $productDetails[$b]->getEpf();
  }
  $totalEpfComp = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalEpfComp += $productDetails[$b]->getEpfComp();
  }

  $totalPerkeso = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalPerkeso += $productDetails[$b]->getPerkeso();
  }
  $totalPerkesoComp = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalPerkesoComp += $productDetails[$b]->getPerkesoComp();
  }

  $totalEis = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalEis += $productDetails[$b]->getEis();
  }
  $totalEisComp = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalEisComp += $productDetails[$b]->getEisComp();
  }
  $totalNetSalary = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalNetSalary += $productDetails[$b]->getNetMonthPay();
  }

  $totalPcb = 0; // initital
  for ($b=0; $b <count($productDetails) ; $b++)
  {
    $totalPcb += $productDetails[$b]->getPcb();
  }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Payment Summary | CMS" />
    <title>Payment Summary | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>


<div class="width100 small-padding">

    <!-- <h1 class="h1-title open no-print" onclick="goBack()"><img src="img/back.png" class="opacity-hover back-icon" alt="Go Back" title="Go Back">Payment Summary</h1> -->
    <h1 class="h1-title">Payment Summary</h1>

    <!-- <a href='uploadExcel.php' class="no-print"><div class="blue-btn width175">Issue Payslip</div></a> -->

    <div class="big-four-input-container no-print">
      <div class="input50-div">
        <p class="input-top-p">Search Staff</p>
        <input type="text" id="myInputB" onkeyup="myFunctionB()" placeholder="Staff Name" class="tele-four-input tele-input clean">
      </div>
    </div>

    <div class="clear"></div>
    <h1 class="text-center gp-h1">ABC Sdn Bhd<br>
    <!-- DECEMBER 2020 - PAYMENT SUMMARY</h1> -->
    <?php echo $month;?> <?php echo $years;?> - PAYMENT SUMMARY</h1>
	<div class="clear"></div>
    <div class="width100 shipping-div2">
  
    <div class="overflow-auto">
        <table class="shipping-table ow-small-table" id="myTable">
            <thead>
               <tr>
                    <th rowspan="2">No.</th>
                    <!-- <th rowspan="2"></th> -->
                    <th rowspan="2">Staff Name</th>
                    <th rowspan="2">Bank Account No.</th>
                    <!-- <th rowspan="2">Status (S/M)</th> -->
                    <th rowspan="2">Marital Status (S/M)</th>
                    <th rowspan="2">Designation</th>
                    <th rowspan="2">EPF No.</th>
                    <th rowspan="2">Salary(RM)</th>
                    <!-- <th rowspan="2">Unpaid Leave (UL)</th> -->
                    <!-- <th rowspan="2">Salary After UL</th> -->
                    
					          <th colspan="2">EPF(RM)</th>
                    <th colspan="2">PERKESO(RM)</th>
                    <th colspan="2">EIS(RM)</th>                    

                    <th rowspan="2">PCB (RM)</th>
                    <th rowspan="2">Allowance</th>
                    <!-- <th rowspan="2">Bonus</th> -->
                    <th rowspan="2">Commission</th>
                    <th rowspan="2">Overtime Payment (OT)</th>
                    <th rowspan="2">Unpaid Leave (UL)</th>
                    <th rowspan="2">Nett Salary (RM)</th>

                    <th rowspan="2" class="no-print">View</th>
                </tr>
                <tr>
                    <th>Employer (13%)</th>
                    <th>Employee (7%)</th>
                    <th>Employer</th>
                    <th>Employee</th>
                    <th>Employer</th>
                    <th>Employee</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <!-- <td></td> -->
                            <td><?php echo $productDetails[$cnt]->getFullname();?></td>
                            <td><?php echo $productDetails[$cnt]->getAccountNo();?></td>
                            <td><?php echo $productDetails[$cnt]->getStatus();?></td>
                            <td><?php echo $productDetails[$cnt]->getDesignation();?></td>
                            <td><?php echo $productDetails[$cnt]->getEpfNo();?></td>
                            <td><?php echo $productDetails[$cnt]->getBasicPay();?></td>
                            <!-- <td><?php echo $productDetails[$cnt]->getUnpaidLeave();?></td> -->
                            <!-- <td><?php echo $productDetails[$cnt]->getSalaryAfterUL();?></td> -->
                            <td><?php echo $productDetails[$cnt]->getEpfComp();?></td>
                            <td><?php echo $productDetails[$cnt]->getEpf();?></td>

                            <td><?php echo $productDetails[$cnt]->getPerkesoComp();?></td>
                            <td><?php echo $productDetails[$cnt]->getPerkeso();?></td>
                            <td><?php echo $productDetails[$cnt]->getEisComp();?></td>
                            <td><?php echo $productDetails[$cnt]->getEis();?></td>
                            <td><?php echo $productDetails[$cnt]->getPcb();?></td>
                            <td><?php echo $productDetails[$cnt]->getAllowance();?></td>
                            <!-- <td><?php echo $productDetails[$cnt]->getBonus();?></td> -->
                            <td><?php echo $productDetails[$cnt]->getCommission();?></td>
                            <td><?php echo $productDetails[$cnt]->getOvertimeCost();?></td>
                            <td><?php echo $productDetails[$cnt]->getUnpaidLeave();?></td>
                            <td><?php echo $nettPay = $productDetails[$cnt]->getNetMonthPay();?></td>

                            <td class="no-print">
                              <form action="adminPayslipView.php" method="POST" target="_blank">
                                  <button class="clean hover1 img-btn" type="submit" name="payment_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                      <img src="img/view.png" class="width100 hover1a">
                                      <img src="img/view2.png" class="width100 hover1b" >
                                  </button>
                              </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
                <tr>
                	  <!-- <td colspan="10" rowspan="2">&nbsp;</td> -->
                    <td colspan="7" rowspan="2">&nbsp;</td>
                     <td><b><?php echo $totalEpfComp;?></b></td>
                     <td><b><?php echo $totalEpf;?></b></td>
                     <td><b><?php echo $totalPerkesoComp;?></b></td>
                     <td><b><?php echo $totalPerkeso;?></b></td>
                     <td><b><?php echo $totalEisComp;?></b></td>
                     <td><b><?php echo $totalEis;?></b></td>
                     <td><b><?php echo $totalPcb;?></b></td>
                     <td colspan="4">&nbsp;</td>
                     <td><b><?php echo $totalNetSalary;?></b></td>
                     <td class="no-print">&nbsp;</td>
                </tr>
                <tr>
                     <td><b>TOTAL EPF PAYMENT</b></td>
                     <td><b><?php echo $totalEpfPayment = $totalEpfComp + $totalEpf;?></b></td>
                     <td><b>TOTAL SOCSO PAYMENT</b></td>
                     <td><b><?php echo $totalPerkesoPayment = $totalPerkesoComp + $totalPerkeso;?></b></td>
                     <td><b>TOTAL EIS PAYMENT</b></td>
                     <td><b><?php echo $totalEisPayment = $totalEisComp + $totalEis;?></b></td>
                     <td colspan="6"></td>
                     <td class="no-print">&nbsp;</td>
                </tr>                
            </tbody>
        </table>
    </div>



</div>
</div>
<style>
.payslip-li{
	color:#264a9c;
	background-color:white;}
.payslip-li .hover1a{
	display:none;}
.payslip-li .hover1b{
	display:block;}
</style>

<?php unset($_SESSION['quotation_session']); unset($_SESSION['invoice_session']); unset($_SESSION['receipt_session']); unset($_SESSION['url']);?>
<?php include 'js.php'; ?>

<!-- <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script> -->

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<!-- <script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script> -->

</body>
</html>