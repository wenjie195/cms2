<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Name.php';
// require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allCategory = getCategory($conn);
$allName = getName($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Add Supplier Name | CMS" />
    <title>Add Supplier Name | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Add Supplier Name</h1> 
    <!-- <form action="utilities/adminAddSupplierName.php" method="POST"> -->
    <form action="utilities/adminAddSupplierNameFunction.php" method="POST">

        <div class="width100">
            <p class="input-title-p">Supplier Name</p>
            <input class="clean tele-input" type="text" placeholder="Supplier Name" id="supplier_name" name="supplier_name" required>        
        </div> 

        <div class="clear"></div>

        <button class="clean red-btn fix300-btn align-left" name="submit">Submit</button>

    </form>

    <div class="clear"></div>


    <div class="overflow-scroll-div margin-top20">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($allName)
                {   
                    for($cnt = 0;$cnt < count($allName) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $allName[$cnt]->getName();?></td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>


</div>

<style>
.telemarketer-li{
	color:#264a9c;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>