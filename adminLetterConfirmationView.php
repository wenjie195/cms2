<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Quotation.php';
require_once dirname(__FILE__) . '/classes/Offer.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Confirmation Letter | CMS" />
    <title>Confirmation Letter | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar next-to-sidebar3 next-to-sidebar3a">
	<div class="page1-print">
	<!-- page1-div <p class="invoice-logo-p text-center"><img src="img/invoice-logo.png" class="invoice-logo" alt="ChiNou Smart Solutions Sdn. Bhd." title="ChiNou Smart Solutions Sdn. Bhd."></p> -->
    <div class="text-center header-div">
<!--        <div class="left-header">
            <img src="img/gp.png" class="header-logo" alt="GP7" title="GP7">
        </div>-->
        <div class="right-header">
            <h3 class="invoice-company-name">ABC SDN. BHD.</h3>
            <h3 class="invoice-company-number invoice-company-number2 margin-bottom5">2021231013066(1369386-H)</h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number margin-bottom2"></h3>
            <h3 class="invoice-company-number"></h3>
        </div>
    </div>
    <div class="clear"></div>
    <?php
    // echo $_POST['letter_uid'];
    if(isset($_POST['letter_uid']))
    {
    $conn = connDB();
    $letterDetails = getOffer($conn,"WHERE uid = ? ", array("uid") ,array($_POST['letter_uid']),"s");
    // $orderId = $ordersDetails[0]->getId();
    ?>

        <p class="ms-p name-p"><?php echo $letterDetails[0]->getName();?></p>
        <p class="ms-p ic-p margin-top5"><?php echo $letterDetails[0]->getIcno();?></p>
        <p class="ms-p margin-top5"><?php echo $letterDetails[0]->getAddressOne();?></p>
        <p class="ms-p add-p"><?php echo $letterDetails[0]->getAddressTwo();?></p>
        <p class="ms-p add-p"><?php echo $letterDetails[0]->getAddressThree();?></p>
        <p class="ms-p add-p"><?php echo $letterDetails[0]->getAddressFour();?></p>

        <div class="clear"></div>

        <p class="ms-p right-date-p">
            <?php echo $date = date("Y-m-d",strtotime($letterDetails[0]->getDateUpdated()));?>
        </p>

        <div class="clear"></div>

        <p class="ms-p"><b><u>CONFIRMATION LETTER FOR THE POST OF <?php echo $letterDetails[0]->getPosition();?> </u></b></p>

        <p class="ms-p margin-top20">We, the management have the pleasure in confirming you,  <?php echo $letterDetails[0]->getName();?>  for the post of  <?php echo $letterDetails[0]->getPosition();?>  after successfully completed  <?php echo $letterDetails[0]->getProbation();?>  probation period with our company. Therefore, your confirmation will be effective on <?php echo $letterDetails[0]->getConfirmDate();?> . </p>

        <p class="ms-p margin-top20">We, the management of GP7 Marketing and Technology Sdn. Bhd. welcome you on board to have a happy and long progressing career together with us. </p>

        <p class="ms-p margin-top20">The terms and condition of your service are as follow:</p> 

        <p class="ms-p margin-top20"><u>SALARY</u></p>

        <p class="ms-p">Your basic salary shall be RM <?php echo $letterDetails[0]->getSalary();?> per month and [ALLOWANCE/BONUS/COMMISSION SCHEME IF ANY] in view of the commitment towards your duty rendered. </p> 

        <p class="ms-p margin-top20"><u>WORKING HOURS AT THE HEAD OFFICE AND BRANCH OFFICES</u></p>

        <p class="ms-p">You shall follow the working hours set by the Company:</p> 

		<table class="time-table">
        	<tbody>
            	<tr>
                	<td>Monday to Friday</td>
                    <td>&nbsp;-&nbsp;</td>
                    <td>10.00am – 7.00pm</td>
                </tr>
             	<tr>
                	<td>Saturday</td>
                    <td>&nbsp;-&nbsp;</td>
                    <td>10.00am – 1.30pm</td>
                </tr>
            	<tr>
                	<td>Lunch Break</td>
                    <td>&nbsp;-&nbsp;</td>
                    <td>1.30pm – 2.30pm</td>
                </tr>                               
            </tbody>
        </table>
<!--        <p class="ms-p margin-top20">Monday to Friday   -     10.00am – 7.00pm</p> 
        <p class="ms-p margin-top5">Saturday           -     10.00am – 1.30pm</p> 
        <p class="ms-p margin-top5">Lunch Break        -     1.30pm – 2.30pm</p> -->

        <p class="ms-p">Absence without approval from the Company will be considered as a serious disciplinary issue and leave shall be deducted from your annual leave entitlement. </p> 

        <p class="ms-p margin-top20"><u>GENERAL CONDUCT</u></p>

        <p class="ms-p">During your employment, your conduct shall be such and not to discredit yourself or your Company and you shall be expected to perform the duties assigned to you in a loyal, efficient, trustworthy and honest manner benefiting your status within the Company.</p> 

        <p class="ms-p margin-top20"><u>SOCIAL MEDIA RESPONSIBILITIES</u></p>

        <p class="ms-p">Any personal comments on social media are strictly personal comments and do not reflect the opinion of the company, thus individual will be solely deemed responsible.</p> 

        <p class="ms-p margin-top20"><u>SECRECY</u></p>

        <p class="ms-p">You shall not, at any time during your employment with the Company or afterwards make public, divulge or disclose to any person any information ad to the business dealings or affairs of the Company which may come to your knowledge in the course of your employment with the Company.</p> 

        <p class="ms-p margin-top20"><u>INTELLECTUAL PROPERTIES</u></p>

        <p class="ms-p">Any Projects or Programs subject done during your tenure, the Intellectual Properties is belong to the company. </p> 
		</div></div>
        <div class="next-to-sidebar  next-to-sidebar3 next-to-sidebar3b">
        <div class="page2-print">
            <div class="text-center header-div web-view-no">
<!--        <div class="left-header">
            <img src="img/gp.png" class="header-logo" alt="GP7" title="GP7">
        </div>-->
        <div class="right-header">
            <h3 class="invoice-company-name">GP SEVEN MARKETING AND TECHNOLOGY SDN. BHD.</h3>
            <h3 class="invoice-company-number invoice-company-number2 margin-bottom5">202001013066(1369386-H)</h3>
            <h3 class="invoice-company-number margin-bottom2">1-12A-12B, SUNTECH@PENANG CYBERCITY, LINTANG MAYANG PASIR 3</h3>
            <h3 class="invoice-company-number margin-bottom2">BANDAR BAYAN LEPAS, 11950, PULAU PINANG MALAYSIA</h3>
            <h3 class="invoice-company-number">TEL: 04-6087149/04-6087150 EMAIL: GP7EVEN@GMAIL.COM</h3>
        </div>
    </div>
    <div class="clear"></div>
        <p class="ms-p margin-top20"><u>LEAVE ENTITLEMENT</u></p>

        <p class="ms-p">You shall be entitled to an annual leave of twelve (12) days accordingly to the Company’s regulation on the completion of twelve (12) month’s service. Subsequently the annual leave shall be dependent upon the number of years as well as your position according to the Company’s regulations. Such annual leave shall be taken at a time to be mutually agreed between yourself and the Company.</p> 

        <p class="ms-p margin-top20"><u>TRANSFER</u></p>

        <p class="ms-p">You may be required to be permanently/ temporary transferred from one section or another department or our Branch Office/ Associate Company to another from time to time at sole discretion of the Company.</p> 

        <p class="ms-p margin-top20"><u>TERMINATION OF EMPLOYMENT</u></p>
		<ol class="term-ol">
        <li>Your period of employment may be terminated by either party in writing by giving one (1) month’s notice or one month salary in lieu after confirmation without assigning any specific reasons. In any case deemed serious by the management, warning will be given to any employees found of wrong doing. Immediate and a final 24-hour dismissal will be adhered to if no changes were made.</li>
        <li>After two (2) years working commitment with the company, you are required to tender two (2) months notice for resignation. For those which are in the executive level at entrance appointment, you are also required to tender two (2) months notice upon resignation. </li>
        <li>Should you tender your resignation from the Company’s service, the Company’s sole obligation shall be to pay the amount of your salary up to date on which your resignation becomes effective.</li>
        <li>Should the Company terminate your employment with notice, the salary earned by you up to date of such termination shall be paid to you.</li>
        <li>In accordance with the Income Tax Act 1976, we shall have to withhold any monies due to you on your resignation or dismissal until tax clearance has been granted by the Director General.</li>
        <li>In the event of your resignation from the Company’s service, any leave which you may have become eligible to (whatever annual or accumulated for overseas travel or both) up to the effective date of your resignation shall be excluded from the period of notice of termination which you are required to serve upon the Company under this agreement, unless specifically agreed by the Company.</li></ol>

        <p class="ms-p margin-top20"><u>OFFICE RULES</u></p>

        <p class="ms-p">Your employment shall be subjected to the Company’s rules and regulations and other procedures as may be made to known to you from time to time. </p> 

        <p class="ms-p margin-top20"><u>ALTERATION</u></p>

        <p class="ms-p">If, for any reasons whatsoever, the Company wishes to alter terms and condition of service in any way, it reserves the right to do so entirely at its own discretion. Any alterations, amendments or additions to these terms shall be advised to you in writing.</p> 


        <p class="ms-p margin-top20">Your signature at the foot of the duplicate copy of this letter shall signify your understandings and acceptance of the condition outline herein which are applicable to your employment. </p> 

        <p class="ms-p margin-top40">Sincerely,</p>
		<div class="left-sign-div">
            <p class="ms-p sign-line"></p>
            <p class="ms-p">Director:</p>
		</div>
		<div class="right-sign-div">
            <p class="ms-p left-signature">Signature: </p>
            <p class="ms-p sign-line right-line"></p>
            <div class="clear"></div>
            <p class="ms-p">Candidate Name: </p>
            <p class="ms-p">Date:</p>
        </div>
		</div>
        <button class="clean red-btn margin-top30 fix300-btn no-print" onclick="window.print()">Generate PDF</button>
            <p class="text-center pointer red-link no-print"><a href="./img/pdf-tutorial.jpg" data-fancybox class="red-link">Tutorial</a></p>

    <?php
    }
    ?>
    
    <div class="clear"></div>

</div>

<!-- <style>
.invoice-li{
	color:#264a9c;
	background-color:white;}
.invoice-li .hover1a{
	display:none;}
.invoice-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

</body>
</html>