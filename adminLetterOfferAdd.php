<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Issue Offer Letter | CMS" />
    <title>Issue Offer Letter | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Issue Offer Letter</h1> 
    <form action="utilities/adminOfferLetterAddFunction.php" method="POST">
    <!-- <form action="#" method="POST"> -->

        <div class="input50-div">
            <p class="input-title-p">Name</p>
            <input class="clean tele-input" type="text" placeholder="Name" id="name" name="name" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">IC Number</p>
            <input class="clean tele-input"  type="text" placeholder="IC Number" id="icno" name="icno" required>        
        </div> 

        <div class="clear"></div>

        <!-- <div class="input50-div">
            <p class="input-title-p">Phone</p>
            <input class="clean tele-input"  type="text" placeholder="Phone" id="phone" name="phone" required>     
        </div> 

        <div class="input50-div second-input50"> -->

        <div class="input50-div">
            <p class="input-title-p">Display Date</p>
            <input class="clean tele-input" type="date" placeholder="Display Date" id="date" name="date" required>        
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Address Line 1</p>     
            <textarea  type="text" class="clean tele-input" placeholder="Address Line 1" id="address_one" name="address_one" required></textarea> 
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Address Line 2</p>     
            <textarea  type="text" class="clean tele-input" placeholder="Address Line 2" id="address_two" name="address_two"></textarea> 
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Address Line 3</p>     
            <textarea  type="text" class="clean tele-input" placeholder="Address Line 3" id="address_three" name="address_three"></textarea> 
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Address Line 4</p>     
            <textarea  type="text" class="clean tele-input" placeholder="Address Line 4" id="address_four" name="address_four"></textarea> 
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Position</p>
            <input class="clean tele-input" type="text" placeholder="Position" id="position" name="position" onkeyup="this.value = this.value.toUpperCase();" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Start Date</p>
            <input class="clean tele-input" type="date" placeholder="Start Date" id="start_date" name="start_date" required>      
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Working Hours (eg. 10am-7pm)</p>     
            <input class="clean tele-input"  type="text" placeholder="Working Hours" id="working_hours" name="working_hours" required>      
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Salary (eg. 3000)</p>     
            <input class="clean tele-input"  type="number" placeholder="Salary" id="salary" name="salary" required>  
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Probation Period (eg. 3 months / 1 years)</p>     
            <input class="clean tele-input"  type="text" placeholder="Probation Period" id="probation" name="probation" required> 
        </div> 

        <div class="input50-div second-input50">  
            <p class="input-title-p">Job Scope</p>     
            <textarea  type="text" class="clean tele-input" placeholder="Job Scope" id="job_scope" name="job_scope" required></textarea> 
        </div> 

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn margin-bottom" name="submit">Submit</button>

        <div class="clear"></div>
    </form>
</div>

<style>
.offer-li{
	color:#264a9c;
	background-color:white;}
.offer-li .hover1a{
	display:none;}
.offer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Fail To Add New Staff !!"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fullname / IC Number has been registered !"; 
        }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Incorrect Password";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>