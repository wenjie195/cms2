<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Name.php';
// require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allName = getName($conn);
// $allCategory = getCategory($conn);

// $timestamp = time();

// if($_SERVER['REQUEST_METHOD'] == 'POST')
// {
//     $supplierName = rewrite($_POST["supplier_name"]);
//     // $registerAttention = rewrite($_POST["register_attention"]);
// }
// else 
// {   }

// $quotationSession = $supplierName.$timestamp;
// $_SESSION['quotation'] = $quotationSession;
$quotationSession = $_SESSION['invoice'];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Invoice (Add Item) | CMS" />
    <title>Invoice (Add Item) | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Add Invoice Details</h1> 
    <!-- <form action="utilities/topUpProductFunction.php" method="POST"> -->
    <!-- <form action="utilities/adminQuotation2Add.php" method="POST"> -->
    <form action="utilities/adminInvoice2AddFunction.php" method="POST">

        <h5 class="h1-title">
            Your Session Name : <?php echo $quotationSession;?>
        </h5> 

        <input class="clean tele-input" type="hidden" value="<?php echo $quotationSession;?>" id="invoice_session" name="invoice_session" readonly>       

        <div class="input50-div">
            <p class="input-title-p">Product Name</p>
            <input class="clean tele-input" type="text" placeholder="Product Name" id="product_name" name="product_name" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Quantity</p>
            <input class="clean tele-input"  type="number" placeholder="Quantity" id="quantity" name="quantity" required>        
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Unit Price (RM)</p>     
            <input class="clean tele-input"  type="text" placeholder="Unit Price" id="unit_price" name="unit_price" required> 
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">UOM</p>
            <input class="clean tele-input"  type="text" placeholder="UOM" id="uom" name="uom">        
        </div> 

        <div class="clear"></div>

        <!-- <div class="input50-div">
            <p class="input-title-p">Total Price</p>
            <input class="clean tele-input"  type="number" placeholder="Total Price" id="total_price" name="total_price" required>        
        </div> 

        <div class="clear"></div> -->

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

        <div class="clear"></div>
    </form>

    <!-- <form action="utilities/adminQuotationCalculation.php" method="POST"> -->
    <form action="utilities/adminInvoiceCalculation.php" method="POST">
        <input class="clean tele-input" type="hidden" value="<?php echo $quotationSession;?>" id="invoice_session" name="invoice_session" readonly>       
        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Finish</button>
    </form>

</div>

<!-- <style>
.account-li{
	color:#264a9c;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

</body>
</html>