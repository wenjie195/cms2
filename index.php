<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Login | CMS" />
    <title>Login | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="width100 overflow">

	<div class="width50 grey-bg right-grey-div">
    	<!--<img src="img/gp.png"  alt="GP7" title="GP7" width="100px">-->
    	<h1 class="tele-h1">Login</h1>
        <p class="login-p margin-bottom">
        	Please enter your ID and password to login.
        </p>
         <form  class="tele-form" action="utilities/loginFunction.php" method="POST">
         	<input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>
         	<input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
            <button class="clean red-btn blue-btn"name="loginButton">Login</button>
         </form>
    </div>
	<div class="width50 left-red-div red-bg">
    	<!--<img src="img/gp.png" class="login-img" alt="Login" title="Login">-->
    </div>    
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Please Register"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Incorrect Password";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Resign <br> Please Contact Admin !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>